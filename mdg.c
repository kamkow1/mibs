// METADATA GENERATOR FOR C LANGUAGE STRUCTURES
// Bootstrap: cc -o mdg mdg.c -lclang
// Use: ./mdg mdg_test.c
//            ^ 
//        source file

#include <clang-c/Index.h>
#define MIBS_IMPL
#define MIBS_REBUILD_ARGS_POST "-lclang"
#include "mibs.h"

Mibs_Default_Allocator alloc = mibs_make_default_allocator();

typedef struct {
    const char *name;
    const char *type;
} Field_MD;

typedef Mibs_Da(Field_MD) Field_MDs;

typedef struct {
    const char *name;
    Field_MDs fields;
} Struct_MD;

typedef Mibs_Da(Struct_MD) Struct_MDs;

typedef struct {
    const char *name;
    const char *type;
} Param_MD;

typedef Mibs_Da(Param_MD) Param_MDs;

typedef struct {
    const char *name;
    const char *rt_type;
    Param_MDs params;
    int is_variadic;
} Func_MD;

typedef Mibs_Da(Func_MD) Func_MDs;

#define struct_mds_deinit(alloc, structs) \
{ \
    for (size_t i = 0; i < (structs)->count; i++) { \
        mibs_free((alloc), (char *)(structs)->items[i].name); \
        for (size_t j = 0; j < (structs)->items[i].fields.count; j++) { \
            mibs_free((alloc), (char*)(structs)->items[i].fields.items[j].name); \
            mibs_free((alloc), (char*)(structs)->items[i].fields.items[j].type); \
        } \
        mibs_da_deinit((alloc), &(structs)->items[i].fields); \
    } \
    mibs_da_deinit((alloc), (structs)); \
}

#define func_mds_deinit(alloc, funcs) \
{ \
    for (size_t i = 0; i < (funcs)->count; i++) { \
        mibs_free((alloc), (char *)(funcs)->items[i].name); \
        mibs_free((alloc), (char *)(funcs)->items[i].rt_type); \
        for (size_t j = 0; j < (funcs)->items[i].params.count; j++) { \
            mibs_free((alloc), (char*)(funcs)->items[i].params.items[j].name); \
            mibs_free((alloc), (char*)(funcs)->items[i].params.items[j].type); \
        } \
        mibs_da_deinit((alloc), &(funcs)->items[i].params); \
    } \
    mibs_da_deinit((alloc), (funcs)); \
}

bool struct_exists(Struct_MDs *structs, const char *name)
{
    for (size_t i = 0; i < structs->count; i++) {
        if (strcmp(name, structs->items[i].name) == 0) return 1;
    }
    return 0;
}

bool field_exists(Field_MDs *fields, const char *name)
{
    for (size_t i = 0; i < fields->count; i++) {
        if (strcmp(name, fields->items[i].name) == 0) return 1;
    }
    return 0;
}

bool func_exists(Func_MDs *funcs, const char *name)
{
    for (size_t i = 0; i < funcs->count; i++) {
        if (strcmp(name, funcs->items[i].name) == 0) return 1;
    }
    return 0;
}

bool param_exists(Param_MDs *params, const char *name)
{
    for (size_t i = 0; i < params->count; i++) {
        if (strcmp(name, params->items[i].name) == 0) return 1;
    }
    return 0;
}

Struct_MDs structs = {0};
Func_MDs funcs = {0};

void make_struct_md(Mibs_String_Builder *struct_md_sb, bool impl_def, const char *unique_file_id)
{
    for (size_t i = 0; i < structs.count; i++) {
        if (impl_def) {
            mibs_sb_append_cstr(&alloc, struct_md_sb, "Struct_MD ");
            mibs_sb_append_cstr(&alloc, struct_md_sb, structs.items[i].name);
            mibs_sb_append_cstr(&alloc, struct_md_sb, "_MD = {\n");
            mibs_sb_append_cstr(&alloc, struct_md_sb, "   .name = \"");
            mibs_sb_append_cstr(&alloc, struct_md_sb, structs.items[i].name);
            mibs_sb_append_cstr(&alloc, struct_md_sb, "\",\n");
            mibs_sb_append_cstr(&alloc, struct_md_sb, "   .nfields = ");
            char num[0xff];
            sprintf(num, "%zu", structs.items[i].fields.count);
            mibs_sb_append_cstr(&alloc, struct_md_sb, num);
            mibs_sb_append_cstr(&alloc, struct_md_sb, ",\n");
            mibs_sb_append_cstr(&alloc, struct_md_sb, "   .fields = {\n");
            for (size_t j = 0; j < structs.items[i].fields.count; j++) {
                mibs_sb_append_cstr(&alloc, struct_md_sb, "      { .name = \"");
                mibs_sb_append_cstr(&alloc, struct_md_sb, structs.items[i].fields.items[j].name);
                mibs_sb_append_cstr(&alloc, struct_md_sb, "\", ");
                mibs_sb_append_cstr(&alloc, struct_md_sb, ".type = \"");
                mibs_sb_append_cstr(&alloc, struct_md_sb, structs.items[i].fields.items[j].type);
                mibs_sb_append_cstr(&alloc, struct_md_sb, "\"},\n");
            }
            mibs_sb_append_cstr(&alloc, struct_md_sb, "   }\n");
            mibs_sb_append_cstr(&alloc, struct_md_sb, "};\n");
        } else {
            mibs_sb_append_cstr(&alloc, struct_md_sb, "extern Struct_MD ");
            mibs_sb_append_cstr(&alloc, struct_md_sb, structs.items[i].name);
            mibs_sb_append_cstr(&alloc, struct_md_sb, "_MD;\n");
        }
    }

    if (impl_def) {
        mibs_sb_append_cstr(&alloc, struct_md_sb, "const char *");
        mibs_sb_append_cstr(&alloc, struct_md_sb, unique_file_id);
        mibs_sb_append_cstr(&alloc, struct_md_sb, "_struct_names");
        mibs_sb_append_cstr(&alloc, struct_md_sb, "[] = {\n");
        for (size_t i = 0; i < structs.count; i++) {
            mibs_sb_append_cstr(&alloc, struct_md_sb, "   \"");
            mibs_sb_append_cstr(&alloc, struct_md_sb, structs.items[i].name);
            mibs_sb_append_cstr(&alloc, struct_md_sb, "\"");
            mibs_sb_append_cstr(&alloc, struct_md_sb, ",\n");
        }
        mibs_sb_append_cstr(&alloc, struct_md_sb, "};\n");
        
        mibs_sb_append_cstr(&alloc, struct_md_sb, "Struct_MD *");
        mibs_sb_append_cstr(&alloc, struct_md_sb, unique_file_id);
        mibs_sb_append_cstr(&alloc, struct_md_sb, "_struct_mds");
        mibs_sb_append_cstr(&alloc, struct_md_sb, "[] = {\n");
        for (size_t i = 0; i < structs.count; i++) {
            mibs_sb_append_cstr(&alloc, struct_md_sb, "   &");
            mibs_sb_append_cstr(&alloc, struct_md_sb, structs.items[i].name);
            mibs_sb_append_cstr(&alloc, struct_md_sb, "_MD");
            mibs_sb_append_cstr(&alloc, struct_md_sb, ",\n");
        }
        mibs_sb_append_cstr(&alloc, struct_md_sb, "};\n");
    } else {
        mibs_sb_append_cstr(&alloc, struct_md_sb, "extern const char *");
        mibs_sb_append_cstr(&alloc, struct_md_sb, unique_file_id);
        mibs_sb_append_cstr(&alloc, struct_md_sb, "_struct_names[");
        char num[0xff];
        sprintf(num, "%zu", structs.count);
        mibs_sb_append_cstr(&alloc, struct_md_sb, num);
        mibs_sb_append_cstr(&alloc, struct_md_sb, "];\n");
        
        mibs_sb_append_cstr(&alloc, struct_md_sb, "extern Struct_MD *");
        mibs_sb_append_cstr(&alloc, struct_md_sb, unique_file_id);
        mibs_sb_append_cstr(&alloc, struct_md_sb, "_struct_mds[");
        mibs_sb_append_cstr(&alloc, struct_md_sb, num);
        mibs_sb_append_cstr(&alloc, struct_md_sb, "];\n");
    }

    mibs_sb_append_null(&alloc, struct_md_sb);
}

void make_func_md(Mibs_String_Builder *func_md_sb, bool impl_def, const char* unique_file_id)
{
    for (size_t i = 0; i < funcs.count; i++) {
        char num[0xff];
        if (impl_def) {
            mibs_sb_append_cstr(&alloc, func_md_sb, "Func_MD ");
            mibs_sb_append_cstr(&alloc, func_md_sb, funcs.items[i].name);
            mibs_sb_append_cstr(&alloc, func_md_sb, "_MD = {\n");
            mibs_sb_append_cstr(&alloc, func_md_sb, "   .name = \"");
            mibs_sb_append_cstr(&alloc, func_md_sb, funcs.items[i].name);
            mibs_sb_append_cstr(&alloc, func_md_sb, "\",\n");
            mibs_sb_append_cstr(&alloc, func_md_sb, "   .is_variadic = ");
            memset(num, 0, 0xff);
            sprintf(num, "%d", funcs.items[i].is_variadic);
            mibs_sb_append_cstr(&alloc, func_md_sb, num);
            mibs_sb_append_cstr(&alloc, func_md_sb, ",\n");
            mibs_sb_append_cstr(&alloc, func_md_sb, "   .rt_type = \"");
            mibs_sb_append_cstr(&alloc, func_md_sb, funcs.items[i].rt_type);
            mibs_sb_append_cstr(&alloc, func_md_sb, "\",\n");
            mibs_sb_append_cstr(&alloc, func_md_sb, "   .nparams = ");
            memset(num, 0, 0xff);
            sprintf(num, "%zu", funcs.items[i].params.count);
            mibs_sb_append_cstr(&alloc, func_md_sb, num);
            mibs_sb_append_cstr(&alloc, func_md_sb, ",\n");
            mibs_sb_append_cstr(&alloc, func_md_sb, "   .params = {\n");
            for (size_t j = 0; j < funcs.items[i].params.count; j++) {
                mibs_sb_append_cstr(&alloc, func_md_sb, "      { .name = \"");
                mibs_sb_append_cstr(&alloc, func_md_sb, funcs.items[i].params.items[j].name);
                mibs_sb_append_cstr(&alloc, func_md_sb, "\", ");
                mibs_sb_append_cstr(&alloc, func_md_sb, ".type = \"");
                mibs_sb_append_cstr(&alloc, func_md_sb, funcs.items[i].params.items[j].type);
                mibs_sb_append_cstr(&alloc, func_md_sb, "\"},\n");
            }
            mibs_sb_append_cstr(&alloc, func_md_sb, "   }\n");
            mibs_sb_append_cstr(&alloc, func_md_sb, "};\n");
        } else {
            mibs_sb_append_cstr(&alloc, func_md_sb, "extern Func_MD ");
            mibs_sb_append_cstr(&alloc, func_md_sb, funcs.items[i].name);
            mibs_sb_append_cstr(&alloc, func_md_sb, "_MD;\n");
        }
    }
    
    if (impl_def) {
        mibs_sb_append_cstr(&alloc, func_md_sb, "const char *");
        mibs_sb_append_cstr(&alloc, func_md_sb, unique_file_id);
        mibs_sb_append_cstr(&alloc, func_md_sb, "_func_names");
        mibs_sb_append_cstr(&alloc, func_md_sb, "[] = {\n");
        for (size_t i = 0; i < funcs.count; i++) {
            mibs_sb_append_cstr(&alloc, func_md_sb, "   \"");
            mibs_sb_append_cstr(&alloc, func_md_sb, funcs.items[i].name);
            mibs_sb_append_cstr(&alloc, func_md_sb, "\"");
            mibs_sb_append_cstr(&alloc, func_md_sb, ",\n");
        }
        mibs_sb_append_cstr(&alloc, func_md_sb, "};\n");
        
        mibs_sb_append_cstr(&alloc, func_md_sb, "Func_MD *");
        mibs_sb_append_cstr(&alloc, func_md_sb, unique_file_id);
        mibs_sb_append_cstr(&alloc, func_md_sb, "_func_mds");
        mibs_sb_append_cstr(&alloc, func_md_sb, "[] = {\n");
        for (size_t i = 0; i < funcs.count; i++) {
            mibs_sb_append_cstr(&alloc, func_md_sb, "   &");
            mibs_sb_append_cstr(&alloc, func_md_sb, funcs.items[i].name);
            mibs_sb_append_cstr(&alloc, func_md_sb, "_MD");
            mibs_sb_append_cstr(&alloc, func_md_sb, ",\n");
        }
        mibs_sb_append_cstr(&alloc, func_md_sb, "};\n");
    } else {
        mibs_sb_append_cstr(&alloc, func_md_sb, "extern const char *");
        mibs_sb_append_cstr(&alloc, func_md_sb, unique_file_id);
        mibs_sb_append_cstr(&alloc, func_md_sb, "_func_names[");
        char num[0xff];
        sprintf(num, "%zu", funcs.count);
        mibs_sb_append_cstr(&alloc, func_md_sb, num);
        mibs_sb_append_cstr(&alloc, func_md_sb, "];\n");
        
        mibs_sb_append_cstr(&alloc, func_md_sb, "extern Func_MD *");
        mibs_sb_append_cstr(&alloc, func_md_sb, unique_file_id);
        mibs_sb_append_cstr(&alloc, func_md_sb, "_func_mds[");
        mibs_sb_append_cstr(&alloc, func_md_sb, num);
        mibs_sb_append_cstr(&alloc, func_md_sb, "];\n");
    }

    mibs_sb_append_null(&alloc, func_md_sb);
}

enum CXChildVisitResult struct_visitor_func(CXCursor current, CXCursor parent, CXClientData data)
{
    if (clang_Location_isFromMainFile(clang_getCursorLocation(current)) == 0) {
        return CXChildVisit_Continue;
    }

    if (clang_getCursorKind(current) == CXCursor_StructDecl) {
        Struct_MD structmd = {0};

        CXType cursor_type = clang_getCursorType(current);
        CXString type_spelling = clang_getTypeSpelling(cursor_type);

        structmd.name = clang_getCString(type_spelling);

        if (!struct_exists(&structs, structmd.name)) {
            mibs_da_append(&alloc, &structs, structmd);
        }
    } else if (clang_getCursorKind(current) == CXCursor_FieldDecl) {
        CXType cursor_type = clang_getCursorType(current);

        CXString type_spelling = clang_getTypeSpelling(cursor_type);
        CXString name_spelling = clang_getCursorSpelling(current);

        Struct_MD *structmd = &structs.items[structs.count-1];
        Field_MD fieldmd = {0};

        fieldmd.type = clang_getCString(type_spelling);
        fieldmd.name = clang_getCString(name_spelling);
        
        if (!field_exists(&structmd->fields, fieldmd.name)) {
            mibs_da_append(&alloc, &structmd->fields, fieldmd);
        }
    }
    return CXChildVisit_Recurse;
}

enum CXChildVisitResult func_visitor_func(CXCursor current, CXCursor parent, CXClientData data)
{
    if (clang_Location_isFromMainFile(clang_getCursorLocation(current)) == 0) {
        return CXChildVisit_Continue;
    }

    if (clang_getCursorKind(current) == CXCursor_FunctionDecl) {
        Func_MD funcmd = {0};

        CXType cursor_type = clang_getCursorType(current);
        CXType rt_type = clang_getResultType(cursor_type);
        CXString rt_type_spelling = clang_getTypeSpelling(rt_type);
        CXString name_spelling = clang_getCursorSpelling(current);

        funcmd.name = clang_getCString(name_spelling);
        funcmd.rt_type = clang_getCString(rt_type_spelling);
        funcmd.is_variadic = clang_Cursor_isVariadic(current);

        if (!func_exists(&funcs, funcmd.name)) {
            mibs_da_append(&alloc, &funcs, funcmd);
        }
    } else if (clang_getCursorKind(current) == CXCursor_ParmDecl) {
        CXType cursor_type = clang_getCursorType(current);

        CXString type_spelling = clang_getTypeSpelling(cursor_type);
        CXString name_spelling = clang_getCursorSpelling(current);

        Func_MD *funcmd = &funcs.items[funcs.count-1];
        Param_MD parammd = {0};

        parammd.type = clang_getCString(type_spelling);
        parammd.name = clang_getCString(name_spelling);
        
        if (!param_exists(&funcmd->params, parammd.name)) {
            mibs_da_append(&alloc, &funcmd->params, parammd);
        }
    }
    return CXChildVisit_Recurse;
}

Mibs_String_Builder get_include_guard(const char* base)
{
    Mibs_String_Builder sb = {0};
    mibs_sb_append_cstr(&alloc, &sb, base);
    mibs_sb_append_null(&alloc, &sb);
    for (size_t i = 0; i < sb.count; i++) {
        char *c = &sb.items[i];
        if (*c == '.') *c = '_';
    }
    return sb;
}

int main(int argc, char ** argv)
{
    mibs_rebuild(&alloc, argc, argv);
    
    mdefer { struct_mds_deinit(&alloc, &structs); }
    mdefer { func_mds_deinit(&alloc, &funcs); }

    CXIndex index = clang_createIndex(0, 0);
    CXTranslationUnit unit = clang_parseTranslationUnit(
        index, argv[1],
        0 /* command_line_args */, 0, /* num_command_line_args */
        0 /* unsaved_files */, 0 /* num_unsaved_files */,
        CXTranslationUnit_None
    );
    if (unit == NULL) {
        mibs_log(MIBS_LL_ERROR, "Failed to parse translation unit\n");
        return 1;
    }

    CXCursor cursor = clang_getTranslationUnitCursor(unit);

    clang_visitChildren(cursor, &struct_visitor_func, 0);
    clang_visitChildren(cursor, &func_visitor_func, 0);

    Mibs_String_Builder file_contents = {0};
    mdefer { mibs_da_deinit(&alloc, &file_contents); }
    
    Mibs_String_Builder h_name = {0};
    mdefer { mibs_da_deinit(&alloc, &h_name); }
    mibs_sb_append_cstr(&alloc, &h_name, argv[1]);
    mibs_da_remove(&h_name, h_name.count-1); // c
    mibs_da_remove(&h_name, h_name.count-1); // .
    mibs_sb_append_cstr(&alloc, &h_name, "_md.h");
    mibs_sb_append_null(&alloc, &h_name);

    Mibs_String_Builder inc_guard = get_include_guard(h_name.items);
    mdefer { mibs_da_deinit(&alloc, &inc_guard); }

    mibs_sb_append_cstr(&alloc, &file_contents, "#ifndef ");
    mibs_sb_append_cstr(&alloc, &file_contents, inc_guard.items);
    mibs_sb_append_cstr(&alloc, &file_contents, "\n");
    mibs_sb_append_cstr(&alloc, &file_contents, "#define ");
    mibs_sb_append_cstr(&alloc, &file_contents, inc_guard.items);
    mibs_sb_append_cstr(&alloc, &file_contents, "\n");
    mibs_sb_append_cstr(&alloc, &file_contents, "#include \"mdg.h\"\n");

    Mibs_String_Builder struct_md_sb = {0};
    mdefer { mibs_da_deinit(&alloc, &struct_md_sb); }
    Mibs_String_Builder func_md_sb = {0};
    mdefer { mibs_da_deinit(&alloc, &func_md_sb); }

    make_struct_md(&struct_md_sb, 0, inc_guard.items);
    make_func_md(&func_md_sb, 0, inc_guard.items);
    mibs_sb_append_cstr(&alloc, &file_contents, struct_md_sb.items);
    mibs_sb_append_cstr(&alloc, &file_contents, func_md_sb.items);
    mibs_sb_append_cstr(&alloc, &file_contents, "#ifdef ");
    mibs_sb_append_cstr(&alloc, &file_contents, inc_guard.items);
    mibs_sb_append_cstr(&alloc, &file_contents, "_impl\n");
    mibs_da_deinit(&alloc, &struct_md_sb);
    mibs_da_deinit(&alloc, &func_md_sb);
    make_struct_md(&struct_md_sb, 1, inc_guard.items);
    make_func_md(&func_md_sb, 1, inc_guard.items);
    mibs_sb_append_cstr(&alloc, &file_contents, struct_md_sb.items);
    mibs_sb_append_cstr(&alloc, &file_contents, func_md_sb.items);
    mibs_sb_append_cstr(&alloc, &file_contents, "#endif // ");
    mibs_sb_append_cstr(&alloc, &file_contents, inc_guard.items);
    mibs_sb_append_cstr(&alloc, &file_contents, "_impl\n");
    
    mibs_sb_append_cstr(&alloc, &file_contents, "#endif // ");
    mibs_sb_append_cstr(&alloc, &file_contents, inc_guard.items);
    mibs_sb_append_cstr(&alloc, &file_contents, "\n");

    mibs_sb_append_null(&alloc, &file_contents);

    FILE *f1 = fopen(h_name.items, "w");
    mdefer { fclose(f1); }
    fwrite(file_contents.items, file_contents.count-1, 1, f1);
    return 0;
}

