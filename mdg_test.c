// cc -o mdg_test mdg_test.c
// ./mdg_test

#define MIBS_IMPL
#include "mibs.h"

#define mdg_test_md_h_impl
#include "mdg_test_md.h"

#include "mdg.h"

typedef struct
{
    int a;
    float b;
    void *c;
} Siema;

typedef struct
{
    int d;
    float e;
    void *f;
} Hello;

typedef struct {
    int aba;
    const char *hohooh;
    Siema s;
} LoooL;

Mibs_Default_Allocator alloc = mibs_make_default_allocator();

const void * some_func(const char *a, Hello *h, ...)
{

}

int main(int argc, char ** argv)
{
#if 0
    // -------------------------------------------------------------------
    // Example of explicitly accessing struct/function metadata
    // -------------------------------------------------------------------

    printf("%s:\n", MD(Siema).name);
    for (size_t i = 0; i < MD(Siema).nfields; i++) {
        printf("    %s %s,\n", MD(Siema).fields[i].type, MD(Siema).fields[i].name);
    }
    printf("%s:\n", MD(LoooL).name);
    for (size_t i = 0; i < MD(LoooL).nfields; i++) {
        printf("    %s %s,\n", MD(LoooL).fields[i].type, MD(LoooL).fields[i].name);
    }

    Mibs_String_Builder text1 = mdg_struct_to_string(&alloc, Siema);
    mdefer { mibs_da_deinit(&alloc, &text1); }
    printf("Siema string: %s\n", text1.items);

    printf("Cmp result: %d\n", mdg_cmp_struct_types(Siema, Hello));
    printf("Cmp result: %d\n", mdg_cmp_struct_types(Siema, LoooL));

    printf("Cmp result: %d\n", mdg_cmp_struct_sem(Siema, Hello));
    printf("Cmp result: %d\n", mdg_cmp_struct_sem(Siema, LoooL));

    printf("%s -> %s:\n", MD(main).name, MD(main).rt_type);
    for (size_t i = 0; i < MD(main).nparams; i++) {
        printf("    %s %s,\n", MD(main).params[i].type, MD(main).params[i].name);
    }
    printf("%s -> %s:\n", MD(some_func).name, MD(some_func).rt_type);
    for (size_t i = 0; i < MD(some_func).nparams; i++) {
        printf("    %s %s,\n", MD(some_func).params[i].type, MD(some_func).params[i].name);
    }
#else
    // -------------------------------------------------------------------
    // Example of implicitly accessing struct/function metadata
    // -------------------------------------------------------------------
    
    for (size_t i = 0; i < mibs_array_len(mdg_struct_names(mdg_test)); i++) {
        const char *name = mdg_struct_names(mdg_test)[i];
        Struct_MD *structmd = mdg_get_struct_md(mdg_test, name);
        printf("typedef struct {\n");
        for (size_t j = 0; j < structmd->nfields; j++) {
            printf("    %s %s;\n", structmd->fields[j].type, structmd->fields[j].name);
        }
        printf("} %s;\n", structmd->name);
    }

    for (size_t i = 0; i < mibs_array_len(mdg_func_names(mdg_test)); i++) {
        const char *name = mdg_func_names(mdg_test)[i];
        Func_MD *funcmd = mdg_get_func_md(mdg_test, name);
        printf("%s %s(", funcmd->rt_type, funcmd->name);
        for (size_t j = 0; j < funcmd->nparams; j++) {
            printf("%s %s", funcmd->params[j].type, funcmd->params[j].name);
            if (j != funcmd->nparams-1) {
                printf(", ");
            }
        }
        if (funcmd->is_variadic) {
            printf(", ...");
        }
        printf(");\n");
    }
#endif

    return 0;    
}

