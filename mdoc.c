// -------------------------------------------------------------------------------------------
// DOCUMENTATION GENERATOR FOR C
// Bootstrap: cc -o mdoc mdoc.c
// Use ./mdoc -i<c source file> -o<output file>
// -------------------------------------------------------------------------------------------

// +mdoc #repo https://gitlab.com/kamkow1/mibs

// +mdoc #intro MDoc - Mibs documentation generator
// +mdoc #intro <h3>Getting started</h3>
// +mdoc #intro Download mdoc.c<br />
// +mdoc #intro <pre><code language="bash">wget https://gitlab.com/kamkow1/mibs/-/raw/master/mdoc.c?ref_type=heads</code></pre><br />
// +mdoc #intro 
// +mdoc #intro Bootstrap MDoc
// +mdoc #intro <pre><code language="bash">cc -o mdoc mdoc.c</code></pre><br />
// +mdoc #intro 
// +mdoc #intro And now you're ready to document your code!
// +mdoc #intro <pre><code language="bash">./mdoc -i[c source file] -o[output file]</code></pre><br />
// +mdoc #intro 
// +mdoc #intro To learn about MDoc syntax, search for one of these terms:
// +mdoc #intro <ul>
// +mdoc #intro <li>MDOC_COMMENT</li>
// +mdoc #intro <li>MDOC_PRAGMA_FUNCTION</li>
// +mdoc #intro <li>MDOC_PRAGMA_PARAM</li>
// +mdoc #intro <li>MDOC_PRAGMA_RETURN</li>
// +mdoc #intro <li>MDOC_PRAGMA_RETURN_NOTE</li>
// +mdoc #intro <li>MDOC_PRAGMA_PARAM_NOTE</li>
// +mdoc #intro <li>MDOC_PRAGMA_MACRO</li>
// +mdoc #intro <li>MDOC_PRAGMA_NOTE</li>
// +mdoc #intro <li>MDOC_PRAGMA_USAGE</li>
// +mdoc #intro <li>MDOC_PRAGMA_VARIADIC</li>
// +mdoc #intro <li>MDOC_PRAGMA_INTRO</li>
// +mdoc #intro <li>MDOC_PRAGMA_REPO</li>
// +mdoc #intro <li>MDOC_PRAGMA_FUNCLIKE</li>
// +mdoc #intro <li>MDOC_PRAGMA_VAR</li>
// +mdoc #intro <li>MDOC_PRAGMA_ENUM</li>
// +mdoc #intro <li>MDOC_PRAGMA_ENUM_ITEM</li>
// +mdoc #intro <li>MDOC_PRAGMA_ENUM_ITEM_NOTE</li>
// +mdoc #intro <li>MDOC_PRAGMA_STRUCT</li>
// +mdoc #intro <li>MDOC_PRAGMA_STRUCT_FIELD</li>
// +mdoc #intro <li>MDOC_PRAGMA_STRUCT_FIELD_NOTE</li>
// +mdoc #intro </ul>
// +mdoc #intro 
// +mdoc #intro 

#define MIBS_REBUILD_ARGS_PRE "-ggdb", "-O3"
#define MIBS_IMPL
#include "mibs.h"

// +mdoc #macro         MDOC_COMMENT
// +mdoc #note          Beginning of an mdoc comment
// +mdoc #usage // +mdoc ...
#define MDOC_COMMENT            "+mdoc"
// +mdoc #macro         MDOC_PRAGMA_FUNCTION
// +mdoc #note          Pragma used to declare a function
// +mdoc #usage // +mdoc #function my_func
#define MDOC_PRAGMA_FUNCTION    "#function"
// +mdoc #macro         MDOC_PRAGMA_PARAM
// +mdoc #note          Pragma used to declare a function or a macro parameter
// +mdoc #usage // Function
// +mdoc #usage 
// +mdoc #usage // +mdoc #param mem/const void*
// +mdoc #usage 
// +mdoc #usage // Macro
// +mdoc #usage // We don't specify the type, because C macro parameters are typeless and the type won't appear in generated HTML
// +mdoc #usage // Use #param_note instead to describe what's meant to be passed into the macro
// +mdoc #usage // +mdoc #param mem/_ 
#define MDOC_PRAGMA_PARAM       "#param"
// +mdoc #macro         MDOC_PRAGMA_RETURN
// +mdoc #note          Pragma used to declare a function return type
// +mdoc #usage // +mdoc #return const char*
#define MDOC_PRAGMA_RETURN      "#return"
// +mdoc #macro         MDOC_PRAGMA_RETURN_NOTE
// +mdoc #note          Pragma used to document a return type of a macro or a function
// +mdoc #usage // Function
// +mdoc #usage 
// +mdoc #usage // +mdoc #function my_func
// +mdoc #usage // +mdoc #return const void*
// +mdoc #usage // +mdoc #return_note Some pointer to memory
// +mdoc #usage 
// +mdoc #usage // Macro
// +mdoc #usage 
// +mdoc #usage // +mdoc #macro my_macro
// +mdoc #usage // We don't need #return, because the type won't appear in generated HTML
// +mdoc #usage // use #return_note to describe what will be returned from the macro
// +mdoc #usage // +mdoc #return_note Some pointer to memory
#define MDOC_PRAGMA_RETURN_NOTE "#return_note"
// +mdoc #macro         MDOC_PRAGMA_PARAM_NOTE
// +mdoc #note          Pragma used to document a parameter of a macro or a function
// +mdoc #usage // +mdoc #param some_weird_parameter/void*
// +mdoc #usage // +mdoc #param_note This is a function/macro parameter that does blah blah blah
#define MDOC_PRAGMA_PARAM_NOTE  "#param_note"
// +mdoc #macro         MDOC_PRAGMA_NOTE
// +mdoc #note          Pragma used to provide a short description
// +mdoc #usage // +mdoc #note This is a note!!
// +mdoc #usage // +mdoc #note This is a continuation of the note. This will appear as a new line!
#define MDOC_PRAGMA_NOTE        "#note"
// +mdoc #macro         MDOC_PRAGMA_MACRO
// +mdoc #note          Used to declare a macro
// +mdoc #usage // +mdoc #macro MY_MACRO
// +mdoc #usage #define MY_MACRO(X,Y,Z)
#define MDOC_PRAGMA_MACRO       "#macro"
// +mdoc #macro         MDOC_PRAGMA_USAGE
// +mdoc #note          Used to generate an example code snippet for a specified item
// +mdoc #note          Like notes, #usage can also be chained into multiple lines of code
// +mdoc #usage // This is example snippet for MDOC_PRAGMA_RETURN_NOTE
// +mdoc #usage 
// +mdoc #usage // +mdoc #usage // Function
// +mdoc #usage // +mdoc #usage 
// +mdoc #usage // +mdoc #usage // +mdoc #function my_func
// +mdoc #usage // +mdoc #usage // +mdoc #return const void*
// +mdoc #usage // +mdoc #usage // +mdoc #return_note Some pointer to memory
// +mdoc #usage // +mdoc #usage 
// +mdoc #usage // +mdoc #usage // Macro
// +mdoc #usage // +mdoc #usage 
// +mdoc #usage // +mdoc #usage // +mdoc #macro my_macro
// +mdoc #usage // +mdoc #usage // We don't need #return, because the type won't appear in generated HTML
// +mdoc #usage // +mdoc #usage // use #return_note to describe what will be returned from the macro
// +mdoc #usage // +mdoc #usage // +mdoc #return_note Some pointer to memory
#define MDOC_PRAGMA_USAGE       "#usage"
// +mdoc #macro         MDOC_PRAGMA_VARIADIC
// +mdoc #note          Use #variadic to denote if a function/macro has variadic parameters
// +mdoc #note          This will add "..." at the end of parameter list of a declaration
// +mdoc #usage // +mdoc #function f
// +mdoc #usage // +mdoc #return void
// +mdoc #usage // +mdoc #param X/int
// +mdoc #usage // +mdoc #variadic
// +mdoc #usage 
// +mdoc #usage // Will result in "void f(int X, ...);"
#define MDOC_PRAGMA_VARIADIC    "#variadic"
// +mdoc #macro         MDOC_PRAGMA_INTRO
// +mdoc #note          Used to declare a simple intro article into the generated docs
// +mdoc #note          You can use HTML within the article contents and it will appear on the website
// +mdoc #note          MDoc uses jquery, highlight.js and bootstrap and you can use it too!
// +mdoc #usage // +mdoc #article This is an article
// +mdoc #usage // +mdoc #article WeeeeWoooo
#define MDOC_PRAGMA_INTRO       "#intro"
// +mdoc #macro         MDOC_PRAGMA_REPO
// +mdoc #note          Add a repository link with an icon at the top of the document
// +mdoc #usage // +mdoc #repo https://gitlab.com/kamkow1/mibs
#define MDOC_PRAGMA_REPO        "#repo"
// +mdoc #macro         MDOC_PRAGMA_FUNCLIKE
// +mdoc #note          Use when declaring a function-like macro
// +mdoc #note          This is only relevant when declaring a function-like macro that doesn't take any arguments
// +mdoc #usage // +mdoc #macro MY_MACRO
// +mdoc #usage // +mdoc #funclike
// +mdoc #usage 
// +mdoc #usage // Will result in "#define MY_MACRO()" instead of "#define MY_MACRO"
#define MDOC_PRAGMA_FUNCLIKE    "#funclike"
// +mdoc #macro         MDOC_PRAGMA_VAR
// +mdoc #note          Used to declare a variable
// +mdoc #usage // +mdoc #var myVariable/const char*
#define MDOC_PRAGMA_VAR         "#var"
// +mdoc #macro         MDOC_PRAGMA_ENUM
// +mdoc #note          Used to declare an enum
// +mdoc #usage // +mdoc #enum My_Cool_Enum
#define MDOC_PRAGMA_ENUM        "#enum"
// +mdoc #macro         MDOC_PRAGMA_ENUM_ITEM
// +mdoc #note          Used to declare an enum item
// +mdoc #usage // +mdoc #enum My_Cool_Enum
// +mdoc #usage // +mdoc #enum_item HELLO
#define MDOC_PRAGMA_ENUM_ITEM   "#enum_item"
// +mdoc #macro         MDOC_PRAGMA_ENUM_ITEM_NOTE
// +mdoc #note          Used to add a note to an enum item
// +mdoc #usage // +mdoc #enum My_Cool_Enum
// +mdoc #usage // +mdoc #enum_item HELLO
// +mdoc #usage // +mdoc #enum_item_note Item says HELLO
#define MDOC_PRAGMA_ENUM_ITEM_NOTE "#enum_item_note"
// +mdoc #macro         MDOC_PRAGMA_STRUCT
// +mdoc #note          Used to declare a structure
// +mdoc #usage // +mdoc #struct My_Struct
#define MDOC_PRAGMA_STRUCT "#struct"
// +mdoc #macro         MDOC_PRAGMA_STRUCT_FIELD
// +mdoc #note          Used tpo declare a structure field
// +mdoc #usage // +mdoc #struct My_Struct
// +mdoc #usage // +mdoc #struct_field hello/int
#define MDOC_PRAGMA_STRUCT_FIELD "#struct_field"
// +mdoc #macro         MDOC_PRAGMA_STRUCT_FIELD_NOTE
// +mdoc #note          Used to add a note to a structure field
// +mdoc #usage // +mdoc #struct My_Struct
// +mdoc #usage // +mdoc #struct_field hello/int
// +mdoc #usage // +mdoc #struct_field_note This field is called "hello"
#define MDOC_PRAGMA_STRUCT_FIELD_NOTE "#struct_field_note"

// +mdoc #var           MDOC_JS_SCRIPT_BASE/const char*
// +mdoc #note          Base of JS script used for the website
const char *MDOC_JS_SCRIPT_BASE = " \
    $(document).ready(function () { \
        $(\"pre code\").each(function(i, e) { \
            hljs.highlightBlock(e); \
        }); \
        $('pre').not('.no-copy').each(function() { \
            $(this).append('<button class=\"copy-btn btn btn-dark\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\" fill=\"currentColor\" class=\"bi bi-clipboard-fill\" viewBox=\"0 0 16 16\"><path fill-rule=\"evenodd\" d=\"M10 1.5a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5zm-5 0A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5v1A1.5 1.5 0 0 1 9.5 4h-3A1.5 1.5 0 0 1 5 2.5zm-2 0h1v1A2.5 2.5 0 0 0 6.5 5h3A2.5 2.5 0 0 0 12 2.5v-1h1a2 2 0 0 1 2 2V14a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V3.5a2 2 0 0 1 2-2\"/></svg></button>'); \
        }); \
        $('pre button').each(function() { \
            $(this).on('click', function() { \
                navigator.clipboard.writeText($(this).parent().children('code').text()); \
            }); \
        }); \
    }); \
    \
    $('#searchbar_form').submit(function(e) { \
        window.location.hash = '#' + $('#searchbar').val(); \
        return false; \
    }); \
";

// +mdoc #var           MDOC_JS_SCRIPT/Mibs_String_Builder
// +mdoc #note          Full JS Script.
// +mdoc #note          We need the text to be dynamic, because we generate terms dynamically for the searchbar
Mibs_String_Builder MDOC_JS_SCRIPT = {0};

// +mdoc #var           MDOC_CSS/const char*
// +mdoc #note          CSS for the generated website
const char *MDOC_CSS = " \
    body { \
        background-color: #181818; \
        overflow: visible; \
    } \
    .doc { \
        max-width: fit-content; \
        margin-left: auto; \
        margin-right: auto; \
        border: 3px dotted var(--bs-gray); \
        padding: 10px; \
        margin-bottom: 5%; \
        margin-top: 5%; \
    } \
    ul { \
        list-style-type: none; \
        padding: 0; \
    } \
    #footer { \
        margin-top: 100px; \
    } \
    hr { \
        border-top: 3px dotted var(--bs-gray); \
    } \
    #searchbar_form { \
        margin-left: auto; \
        margin-right: auto; \
        width: 40%; \
    } \
    .copy-btn { \
        margin: 10px; \
    } \
";

// +mdoc #var           alloc/Mibs_Arena_Allocator
// +mdoc #note          Global allocator
Mibs_Arena_Allocator alloc = mibs_make_arena_allocator(1024*1024*8);
// +mdoc #var           g_argc/int
// +mdoc #note          number of commandline arguments; Needed to generate commandline in the footer
int g_argc;
// +mdoc #var           g_argv/char**
// +mdoc #note          array of commandline arguments; Needed to generate commandline in the footer
char **g_argv;

// +mdoc #enum          Current_Item_Type
// +mdoc #note          The type of the currently constructed item.
// +mdoc #note          When process_comments() encounters for eg. #function, it will append to a function array
// +mdoc #note          and set the current item type, so next pragmas will know if they should add their data
// +mdoc #note          to a function, macro or a variable
// +mdoc #enum_item     CITFUNCTION
// +mdoc #enum_item_note #function
// +mdoc #enum_item     CITMACRO
// +mdoc #enum_item_note #macro
// +mdoc #enum_item     CITVAR
// +mdoc #enum_item_note #var
// +mdoc #enum_item     CITENUM
// +mdoc #enum_item_note #enum
typedef enum {
    CITFUNCTION,
    CITMACRO,
    CITVAR,
    CITENUM,
    CITSTRUCT,
} Current_Item_Type;

// +mdoc #struct        MDoc_Function_Param
// +mdoc #note          Data for a function parameter
// +mdoc #struct_field  name/const char*
// +mdoc #struct_field  type/const char*
// +mdoc #struct_field  param_note/const char*
typedef struct {
    const char* name, *type, *param_note;
} MDoc_Function_Param;

typedef MDoc_Function_Param MDoc_Function_Params[0xff];

// +mdoc #struct        MDoc_Function
// +mdoc #note          Data for a function
// +mdoc #struct_field  name/const char*
// +mdoc #struct_field  rt_type/const char*
// +mdoc #struct_field  rt_type_note/const char*
// +mdoc #struct_field  nparams/int
// +mdoc #struct_field  params/MDoc_Function_Params
// +mdoc #struct_field  note/Mibs_String_Builder
// +mdoc #struct_field  usage/Mibs_String_Builder
// +mdoc #struct_field  has_notes/bool
// +mdoc #struct_field  is_variadic/bool
typedef struct {
    const char* name, *rt_type, *rt_type_note;
    int nparams;
    MDoc_Function_Params params;
    bool has_notes, is_variadic;
    Mibs_String_Builder note, usage;
} MDoc_Function;

// +mdoc #struct        MDoc_Macro
// +mdoc #note          Data for a macro
// +mdoc #struct_field  name/const char*
// +mdoc #struct_field  rt_type/const char*
// +mdoc #struct_field  rt_type_note/const char*
// +mdoc #struct_field  nparams/int
// +mdoc #struct_field  params/MDoc_Function_Params
// +mdoc #struct_field  note/Mibs_String_Builder
// +mdoc #struct_field  usage/Mibs_String_Builder
// +mdoc #struct_field  is_funclike/bool
// +mdoc #struct_field  has_notes/bool
// +mdoc #struct_field  is_variadic/bool
typedef struct {
    const char* name, *rt_type, *rt_type_note;
    int nparams;
    MDoc_Function_Params params;
    Mibs_String_Builder note, usage;
    bool is_funclike, has_notes, is_variadic;
} MDoc_Macro;

// +mdoc #struct        MDoc_Var
// +mdoc #note          Data for a variable
// +mdoc #struct_field  name/const char*
// +mdoc #struct_field  type/const char*
// +mdoc #struct_field  note/Mibs_String_Builder
// +mdoc #struct_field  usage/Mibs_String_Builder
typedef struct {
    const char* name, *type;
    Mibs_String_Builder note, usage;
} MDoc_Var;

// +mdoc #struct        MDoc_Enum_Item
// +mdoc #note          Data for an enumeration item
// +mdoc #struct_field  name/const char*
// +mdoc #struct_field  note/Mibs_String_Builder
typedef struct {
    const char* name;
    Mibs_String_Builder note;
} MDoc_Enum_Item;

typedef MDoc_Enum_Item MDoc_Enum_Items[0xff];

// +mdoc #struct        MDoc_Enum
// +mdoc #note          Data for an enumeration
// +mdoc #struct_field  name/const char*
// +mdoc #struct_field  nitems/int
// +mdoc #struct_field  items/MDoc_Enum_Items
// +mdoc #struct_field  note/Mibs_String_Builder
// +mdoc #struct_field  usage/Mibs_String_Builder
typedef struct {
    const char* name;
    int nitems;
    MDoc_Enum_Items items;
    Mibs_String_Builder note, usage;
} MDoc_Enum;

// +mdoc #struct        MDoc_Struct_Field
// +mdoc #note          Data for a structure field
// +mdoc #struct_field  name/const char*
// +mdoc #struct_field  type/const char*
// +mdoc #struct_field  note/Mibs_String_Builder
typedef struct {
    const char* name, *type;
    Mibs_String_Builder note;
} MDoc_Struct_Field;

typedef MDoc_Struct_Field MDoc_Struct_Fields[0xff];

// +mdoc #struct        MDoc_Struct
// +mdoc #note          Data for a structure
// +mdoc #struct_field  note/Mibs_String_Builder
// +mdoc #struct_field  usage/Mibs_String_Builder
// +mdoc #struct_field  name/const char*
// +mdoc #struct_field  nfields/int
// +mdoc #struct_field  fields/MDoc_Struct_Fields
typedef struct {
    Mibs_String_Builder note, usage;
    const char* name;
    int nfields;
    MDoc_Struct_Fields fields;
} MDoc_Struct;

typedef Mibs_Da(MDoc_Function) MDoc_Functions;
typedef Mibs_Da(MDoc_Macro) MDoc_Macros;
typedef Mibs_Da(MDoc_Var) MDoc_Vars;
typedef Mibs_Da(MDoc_Enum) MDoc_Enums;
typedef Mibs_Da(MDoc_Struct) MDoc_Structs;
typedef Mibs_String_Builder MDoc_Intro;
typedef Mibs_String_Builder MDoc_Repo;

// +mdoc #function      string_starts_with
// +mdoc #note          Utility function to check whether a string starts with a perticular substring
// +mdoc #return        bool
// +mdoc #param         a/const char*
// +mdoc #param         b/const char*
bool string_starts_with(const char *a, const char* b)
{
    return !strncmp(a,b,strlen(b));
}

// +mdoc #function      generate_html_links_and_scripts
// +mdoc #note          Generate link and script tags with external CSS and JavaScript
// +mdoc #note          MDoc uses highlight.js, Bootstrap, JQuery and JQueryUI
// +mdoc #return        void
// +mdoc #param         html/Mibs_String_Builder*
void generate_html_links_and_scripts(Mibs_String_Builder *html)
{
    mibs_sb_append_cstr(&alloc, html, "<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.3.3/css/bootstrap.min.css\" integrity=\"sha512-jnSuA4Ss2PkkikSOLtYs8BlYIeeIK1h99ty4YfvRPAlzr377vr3CXDb7sb7eEEBYjDtcYj+AjBH3FLv5uSJuXg==\" crossorigin=\"anonymous\" referrerpolicy=\"no-referrer\" />");
    mibs_sb_append_cstr(&alloc, html, "<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/styles/default.min.css\">");
    mibs_sb_append_cstr(&alloc, html, "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/highlight.min.js\"></script>");
    mibs_sb_append_cstr(&alloc, html, "<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/styles/vs2015.min.css\" />");
    mibs_sb_append_cstr(&alloc, html, "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js\" integrity=\"sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==\" crossorigin=\"anonymous\" referrerpolicy=\"no-referrer\"></script>");
    mibs_sb_append_cstr(&alloc, html, "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.13.3/jquery-ui.min.js\" integrity=\"sha512-Ww1y9OuQ2kehgVWSD/3nhgfrb424O3802QYP/A5gPXoM4+rRjiKrjHdGxQKrMGQykmsJ/86oGdHszfcVgUr4hA==\" crossorigin=\"anonymous\" referrerpolicy=\"no-referrer\"></script>");
mibs_sb_append_cstr(&alloc, html, "<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.13.3/themes/black-tie/jquery-ui.min.css\" integrity=\"sha512-Y/Kw1+6l0ZLMBdoMy+rgIppt7a0wqfVN+2xAp/1F9Q8NIza7aO5mmWhw/Vy4AWAxBhFgLIypbxB5els4tXhHZQ==\" crossorigin=\"anonymous\" referrerpolicy=\"no-referrer\" />");
}

// +mdoc #function      generate_html_css
// +mdoc #note          Generate &lt;style&gt; tag containing content of MDOC_CSS variable
// +mdoc #return        void
// +mdoc #param         html/Mibs_String_Builder*
void generate_html_css(Mibs_String_Builder *html)
{
    mibs_sb_append_cstr(&alloc, html, "<style>");
    mibs_sb_append_cstr(&alloc, html, MDOC_CSS);
    mibs_sb_append_cstr(&alloc, html, "</style>");
}

// +mdoc #function      generate_html_js_script
// +mdoc #note          Generate &lt;script&gt; tag containing content of MDOC_JS_SCRIPT variable
// +mdoc #return        void
// +mdoc #param         html/Mibs_String_Builder*
void generate_html_js_script(Mibs_String_Builder *html)
{
    mibs_sb_append_cstr(&alloc, html, "<script>");
    {
        mibs_sb_append_cstr(&alloc, html, MDOC_JS_SCRIPT.items);
    }
    mibs_sb_append_cstr(&alloc, html, "</script>");
}

// +mdoc #function      generate_html_function_doc
// +mdoc #note          Generate documentation block (&lt;li&gt; element) for a given MDoc_Function instance
// +mdoc #return        void
// +mdoc #param         html/Mibs_String_Builder*
// +mdoc #param         function/MDoc_Function*
void generate_html_function_doc(Mibs_String_Builder *html, MDoc_Function *function)
{
    mibs_sb_append_cstr(&alloc, html, "<li id=\"");
    mibs_sb_append_cstr(&alloc, html, function->name);
    mibs_sb_append_cstr(&alloc, html, "\">");
    {
        if (function->has_notes) {
            mibs_sb_append_cstr(&alloc, html, "<p class=\"text-light\">");
            {
                mibs_sb_append_cstr(&alloc, html, "Notes: "MIBS_NL);
                mibs_sb_append_cstr(&alloc, html, "<p class=\"text-light\">");
                {
                    if (function->note.count > 0) {
                        mibs_sb_append_null(&alloc, &function->note);
                        mibs_sb_append_cstr(&alloc, html, function->note.items);
                        mibs_sb_append_cstr(&alloc, html, MIBS_NL);
                    }

                    mibs_sb_append_cstr(&alloc, html, "<ul>");
                    {
                        mibs_sb_append_cstr(&alloc, html, "<li class=\"text-light\">");
                        {
                            if (function->rt_type_note) {
                                mibs_sb_append_cstr(&alloc, html, "Return: ");
                                mibs_sb_append_cstr(&alloc, html, "<span class=\"text-light\">");
                                {
                                    mibs_sb_append_cstr(&alloc, html, function->rt_type_note);
                                }
                                mibs_split_cstr(&alloc, html, "</span>");
                            }
                        }
                        mibs_sb_append_cstr(&alloc, html, "</li>");

                        for (size_t i = 0; i < function->nparams; i++) {
                            mibs_sb_append_cstr(&alloc, html, "<li class=\"text-light\">");
                            {
                                if (function->params[i].param_note) {
                                    mibs_sb_append_cstr(&alloc, html, "Parameter ");
                                    mibs_sb_append_cstr(&alloc, html, function->params[i].name);
                                    mibs_sb_append_cstr(&alloc, html, ": ");
                                    mibs_sb_append_cstr(&alloc, html, "<span class=\"text-light\">");
                                    {
                                        mibs_sb_append_cstr(&alloc, html, function->params[i].param_note);
                                    }
                                    mibs_split_cstr(&alloc, html, "</span>");
                                }
                            }
                            mibs_sb_append_cstr(&alloc, html, "</li>");
                        }
                    }
                    mibs_sb_append_cstr(&alloc, html, "</ul>");
                }
                mibs_sb_append_cstr(&alloc, html, "</p>");
            }
            mibs_sb_append_cstr(&alloc, html, "</p>");
        }

        mibs_sb_append_cstr(&alloc, html, "<pre class=\"no-copy\"><code class=\"c\">");
        {
            size_t begin = strlen(function->rt_type) + 1 + strlen(function->name) + 2;
            mibs_sb_append_cstr(&alloc, html, function->rt_type);
            mibs_sb_append_cstr(&alloc, html, " ");
            mibs_sb_append_cstr(&alloc, html, function->name);
            mibs_sb_append_cstr(&alloc, html, " ");
            mibs_sb_append_cstr(&alloc, html, "(");
            for (size_t j = 0; j < function->nparams; j++) {
                mibs_sb_append_cstr(&alloc, html, function->params[j].type);
                mibs_sb_append_cstr(&alloc, html, " ");
                mibs_sb_append_cstr(&alloc, html, function->params[j].name);
                if (j != function->nparams-1) {
                    mibs_sb_append_cstr(&alloc, html, ", ");
                    if (j > 1) {
                        mibs_sb_append_cstr(&alloc, html, MIBS_NL);
                        for (size_t k = 0; k < begin/4; k++) {
                            mibs_sb_append_cstr(&alloc, html, "    ");
                        }
                    }
                }
            }
            if (function->is_variadic) {
                mibs_sb_append_cstr(&alloc, html, ", ...");
            }
            mibs_sb_append_cstr(&alloc, html, ")");
            mibs_sb_append_cstr(&alloc, html, ";");
        }
        mibs_sb_append_cstr(&alloc, html, "</code></pre>");
        if (function->usage.count > 0) {
            mibs_sb_append_cstr(&alloc, html, "<p class=\"text-light\">Usage:</p>");
            mibs_sb_append_cstr(&alloc, html, "<pre><code class=\"c\">");
            {
                mibs_sb_append_null(&alloc, &function->usage);
                mibs_sb_append_cstr(&alloc,  html, function->usage.items);
            }
            mibs_sb_append_cstr(&alloc, html, "</code></pre>");
        }
    }
    mibs_sb_append_cstr(&alloc, html, "</li>");
}

// +mdoc #function      generate_html_var_doc
// +mdoc #note          Generate documentation block (&lt;li&gt; element) for a given MDoc_Var instance
// +mdoc #return        void
// +mdoc #param         html/Mibs_String_Builder*
// +mdoc #param         var/MDoc_Var*
void generate_html_var_doc(Mibs_String_Builder *html, MDoc_Var *var)
{
    mibs_sb_append_cstr(&alloc, html, "<li id=\"");
    mibs_sb_append_cstr(&alloc, html, var->name);
    mibs_sb_append_cstr(&alloc, html, "\">");
    {
        if (var->note.count > 0) {
            mibs_sb_append_cstr(&alloc, html, "<p class=\"text-light\">");
            {
                mibs_sb_append_cstr(&alloc, html, "Notes: "MIBS_NL);
                mibs_sb_append_cstr(&alloc, html, "<p class=\"text-light\">");
                {
                    mibs_sb_append_null(&alloc, &var->note);
                    mibs_sb_append_cstr(&alloc, html, var->note.items);
                    mibs_sb_append_cstr(&alloc, html, MIBS_NL);
                }
                mibs_sb_append_cstr(&alloc, html, "</p>");
            }
            mibs_sb_append_cstr(&alloc, html, "</p>");
        }

        mibs_sb_append_cstr(&alloc, html, "<pre class=\"no-copy\"><code class=\"c\">");
        {
            mibs_sb_append_cstr(&alloc, html, var->type);
            mibs_sb_append_cstr(&alloc, html, " ");
            mibs_sb_append_cstr(&alloc, html, var->name);
            mibs_sb_append_cstr(&alloc, html, ";");
        }
        mibs_sb_append_cstr(&alloc, html, "</code></pre>");
        if (var->usage.count > 0) {
            mibs_sb_append_cstr(&alloc, html, "<p class=\"text-light\">Usage:</p>");
            mibs_sb_append_cstr(&alloc, html, "<pre><code class=\"c\">");
            {
                mibs_sb_append_null(&alloc, &var->usage);
                mibs_sb_append_cstr(&alloc,  html, var->usage.items);
            }
            mibs_sb_append_cstr(&alloc, html, "</code></pre>");
        }
    }
    mibs_sb_append_cstr(&alloc, html, "</li>");
}

// +mdoc #function      generate_html_macro_doc
// +mdoc #note          Generate documentation block (&lt;li&gt; element) for a given MDoc_Macro instance
// +mdoc #return        void
// +mdoc #param         html/Mibs_String_Builder*
// +mdoc #param         macro/MDoc_Macro*
void generate_html_macro_doc(Mibs_String_Builder *html, MDoc_Macro *macro)
{
    mibs_sb_append_cstr(&alloc, html, "<li id=\"");
    mibs_sb_append_cstr(&alloc, html, macro->name);
    mibs_sb_append_cstr(&alloc, html, "\">");
    {
        if (macro->has_notes) {
            mibs_sb_append_cstr(&alloc, html, "<p class=\"text-light\">");
            {
                mibs_sb_append_cstr(&alloc, html, "Notes: "MIBS_NL);
                mibs_sb_append_cstr(&alloc, html, "<p class=\"text-light\">");
                {
                    if (macro->note.count > 0) {
                        mibs_sb_append_null(&alloc, &macro->note);
                        mibs_sb_append_cstr(&alloc, html, macro->note.items);
                        mibs_sb_append_cstr(&alloc, html, MIBS_NL);
                    }

                    mibs_sb_append_cstr(&alloc, html, "<ul>");
                    {
                        mibs_sb_append_cstr(&alloc, html, "<li class=\"text-light\">");
                        {
                            if (macro->rt_type_note) {
                                mibs_sb_append_cstr(&alloc, html, "Return: ");
                                mibs_sb_append_cstr(&alloc, html, "<span class=\"text-light\">");
                                {
                                    mibs_sb_append_cstr(&alloc, html, macro->rt_type_note);
                                }
                                mibs_split_cstr(&alloc, html, "</span>");
                            }
                        }
                        mibs_sb_append_cstr(&alloc, html, "</li>");

                        for (size_t i = 0; i < macro->nparams; i++) {
                            mibs_sb_append_cstr(&alloc, html, "<li class=\"text-light\">");
                            {
                                if (macro->params[i].param_note) {
                                    mibs_sb_append_cstr(&alloc, html, "Parameter ");
                                    mibs_sb_append_cstr(&alloc, html, macro->params[i].name);
                                    mibs_sb_append_cstr(&alloc, html, ": ");
                                    mibs_sb_append_cstr(&alloc, html, "<span class=\"text-light\">");
                                    {
                                        mibs_sb_append_cstr(&alloc, html, macro->params[i].param_note);
                                    }
                                    mibs_split_cstr(&alloc, html, "</span>");
                                }
                            }
                            mibs_sb_append_cstr(&alloc, html, "</li>");
                        }
                    }
                    mibs_sb_append_cstr(&alloc, html, "</ul>");
                }
                mibs_sb_append_cstr(&alloc, html, "</p>");
            }
            mibs_sb_append_cstr(&alloc, html, "</p>");
        }

        mibs_sb_append_cstr(&alloc, html, "<pre class=\"no-copy\"><code class=\"c\">");
        {
            mibs_sb_append_cstr(&alloc, html, "#define");
            mibs_sb_append_cstr(&alloc, html, " ");
            mibs_sb_append_cstr(&alloc, html, macro->name);
            if (macro->nparams > 0 || macro->is_funclike) {
                mibs_sb_append_cstr(&alloc, html, "(");
                for (size_t j = 0; j < macro->nparams; j++) {
                    mibs_sb_append_cstr(&alloc, html, macro->params[j].name);
                    if (j != macro->nparams-1) {
                        mibs_sb_append_cstr(&alloc, html, ", ");
                    }
                }
                if (macro->is_variadic) {
                    mibs_sb_append_cstr(&alloc, html, ", ...");
                }
                mibs_sb_append_cstr(&alloc, html, ")");
            }
        }
        mibs_sb_append_cstr(&alloc, html, "</code></pre>");
        if (macro->usage.count > 0) {
            mibs_sb_append_cstr(&alloc, html, "<p class=\"text-light\">Usage:</p>");
            mibs_sb_append_cstr(&alloc, html, "<pre><code class=\"c\">");
            {
                mibs_sb_append_null(&alloc, &macro->usage);
                mibs_sb_append_cstr(&alloc,  html, macro->usage.items);
            }
            mibs_sb_append_cstr(&alloc, html, "</code></pre>");
        }
    }
    mibs_sb_append_cstr(&alloc, html, "</li>");
}


// +mdoc #function      generate_html_enum_doc
// +mdoc #note          Generate documentation block (&lt;li&gt; element) for a given MDoc_Enum instance
// +mdoc #return        void
// +mdoc #param         html/Mibs_String_Builder*
// +mdoc #param         _enum/MDoc_Enum*
void generate_html_enum_doc(Mibs_String_Builder *html, MDoc_Enum *_enum)
{
    mibs_sb_append_cstr(&alloc, html, "<li id=\"");
    mibs_sb_append_cstr(&alloc, html, _enum->name);
    mibs_sb_append_cstr(&alloc, html, "\">");
    {
        mibs_sb_append_cstr(&alloc, html, "<p class=\"text-light\">");
        {
            mibs_sb_append_cstr(&alloc, html, "Notes: "MIBS_NL);
            mibs_sb_append_cstr(&alloc, html, "<p class=\"text-light\">");
            {
                if (_enum->note.count > 0) {
                    mibs_sb_append_null(&alloc, &_enum->note);
                    mibs_sb_append_cstr(&alloc, html, _enum->note.items);
                    mibs_sb_append_cstr(&alloc, html, MIBS_NL);
                }

                mibs_sb_append_cstr(&alloc, html, "<ul>");
                {
                    for (size_t i = 0; i < _enum->nitems; i++) {
                        mibs_sb_append_cstr(&alloc, html, "<li class=\"text-light\">");
                        {
                            if (_enum->items[i].note.count > 0) {
                                mibs_sb_append_cstr(&alloc, html, "Item ");
                                mibs_sb_append_cstr(&alloc, html, _enum->items[i].name);
                                mibs_sb_append_cstr(&alloc, html, ": ");
                                mibs_sb_append_cstr(&alloc, html, "<span class=\"text-light\">");
                                {
                                    mibs_sb_append_null(&alloc, &_enum->items[i].note);
                                    mibs_sb_append_cstr(&alloc, html, _enum->items[i].note.items);
                                }
                                mibs_split_cstr(&alloc, html, "</span>");
                            }
                        }
                        mibs_sb_append_cstr(&alloc, html, "</li>");
                    }
                }
                mibs_sb_append_cstr(&alloc, html, "</ul>");
            }
            mibs_sb_append_cstr(&alloc, html, "</p>");
        }
        mibs_sb_append_cstr(&alloc, html, "</p>");

        mibs_sb_append_cstr(&alloc, html, "<pre class=\"no-copy\"><code class=\"c\">");
        {
            mibs_sb_append_cstr(&alloc, html, "typedef enum {"MIBS_NL);
            for (size_t j = 0; j < _enum->nitems; j++) {
                mibs_sb_append_cstr(&alloc, html, "    ");
                mibs_sb_append_cstr(&alloc, html, _enum->items[j].name);
                if (j != _enum->nitems-1) {
                    mibs_sb_append_cstr(&alloc, html, ", ");
                }
                mibs_sb_append_cstr(&alloc, html, MIBS_NL);
            }
            mibs_sb_append_cstr(&alloc, html, "} ");
            mibs_sb_append_cstr(&alloc, html, _enum->name);
            mibs_sb_append_cstr(&alloc, html, ";");
        }
        mibs_sb_append_cstr(&alloc, html, "</code></pre>");
        if (_enum->usage.count > 0) {
            mibs_sb_append_cstr(&alloc, html, "<p class=\"text-light\">Usage:</p>");
            mibs_sb_append_cstr(&alloc, html, "<pre><code class=\"c\">");
            {
                mibs_sb_append_null(&alloc, &_enum->usage);
                mibs_sb_append_cstr(&alloc,  html, _enum->usage.items);
            }
            mibs_sb_append_cstr(&alloc, html, "</code></pre>");
        }
    }
    mibs_sb_append_cstr(&alloc, html, "</li>");
}

// +mdoc #function      generate_html_struct_doc
// +mdoc #note          Generate documentation block (&lt;li&gt; element) for a given MDoc_Struct instance
// +mdoc #return        void
// +mdoc #param         html/Mibs_String_Builder*
// +mdoc #param         _struct/MDoc_Struct*
void generate_html_struct_doc(Mibs_String_Builder *html, MDoc_Struct *_struct)
{
    mibs_sb_append_cstr(&alloc, html, "<li id=\"");
    mibs_sb_append_cstr(&alloc, html, _struct->name);
    mibs_sb_append_cstr(&alloc, html, "\">");
    {
        mibs_sb_append_cstr(&alloc, html, "<p class=\"text-light\">");
        {
            mibs_sb_append_cstr(&alloc, html, "Notes: "MIBS_NL);
            mibs_sb_append_cstr(&alloc, html, "<p class=\"text-light\">");
            {
                if (_struct->note.count > 0) {
                    mibs_sb_append_null(&alloc, &_struct->note);
                    mibs_sb_append_cstr(&alloc, html, _struct->note.items);
                    mibs_sb_append_cstr(&alloc, html, MIBS_NL);
                }

                mibs_sb_append_cstr(&alloc, html, "<ul>");
                {
                    for (size_t i = 0; i < _struct->nfields; i++) {
                        mibs_sb_append_cstr(&alloc, html, "<li class=\"text-light\">");
                        {
                            if (_struct->fields[i].note.count > 0) {
                                mibs_sb_append_cstr(&alloc, html, "Field ");
                                mibs_sb_append_cstr(&alloc, html, _struct->fields[i].name);
                                mibs_sb_append_cstr(&alloc, html, ": ");
                                mibs_sb_append_cstr(&alloc, html, "<span class=\"text-light\">");
                                {
                                    mibs_sb_append_null(&alloc, &_struct->fields[i].note);
                                    mibs_sb_append_cstr(&alloc, html, _struct->fields[i].note.items);
                                }
                                mibs_split_cstr(&alloc, html, "</span>");
                            }
                        }
                        mibs_sb_append_cstr(&alloc, html, "</li>");
                    }
                }
                mibs_sb_append_cstr(&alloc, html, "</ul>");
            }
            mibs_sb_append_cstr(&alloc, html, "</p>");
        }
        mibs_sb_append_cstr(&alloc, html, "</p>");

        mibs_sb_append_cstr(&alloc, html, "<pre class=\"no-copy\"><code class=\"c\">");
        {
            mibs_sb_append_cstr(&alloc, html, "typedef struct {"MIBS_NL);
            for (size_t j = 0; j < _struct->nfields; j++) {
                mibs_sb_append_cstr(&alloc, html, "    ");
                mibs_sb_append_cstr(&alloc, html, _struct->fields[j].type);
                mibs_sb_append_cstr(&alloc, html, " ");
                mibs_sb_append_cstr(&alloc, html, _struct->fields[j].name);
                mibs_sb_append_cstr(&alloc, html, ";");
                mibs_sb_append_cstr(&alloc, html, MIBS_NL);
            }
            mibs_sb_append_cstr(&alloc, html, "} ");
            mibs_sb_append_cstr(&alloc, html, _struct->name);
            mibs_sb_append_cstr(&alloc, html, ";");
        }
        mibs_sb_append_cstr(&alloc, html, "</code></pre>");
        if (_struct->usage.count > 0) {
            mibs_sb_append_cstr(&alloc, html, "<p class=\"text-light\">Usage:</p>");
            mibs_sb_append_cstr(&alloc, html, "<pre><code class=\"c\">");
            {
                mibs_sb_append_null(&alloc, &_struct->usage);
                mibs_sb_append_cstr(&alloc,  html, _struct->usage.items);
            }
            mibs_sb_append_cstr(&alloc, html, "</code></pre>");
        }
    }
    mibs_sb_append_cstr(&alloc, html, "</li>");
}

// +mdoc #function          generate_html_page_title
// +mdoc #note              Generate &lt;title&gt; tag containing the file path from which the docs were generated
// +mdoc #return            void
// +mdoc #param             html/Mibs_String_Builder*
// +mdoc #param             file_path/const char*
void generate_html_page_title(Mibs_String_Builder *html, const char* file_path)
{
    mibs_sb_append_cstr(&alloc, html, "<title>");
    {
        mibs_sb_append_cstr(&alloc, html, file_path);
        mibs_sb_append_cstr(&alloc, html, " [MDoc Documentation]");
    }
    mibs_sb_append_cstr(&alloc, html, "</title>");
}

// +mdoc #function          generate_html_head
// +mdoc #note              Generate &lt;head&gt; tag containing the page title, external CSS and JS
// +mdoc #return            void
// +mdoc #param             html/Mibs_String_Builder*
// +mdoc #param             file_path/const char*
void generate_html_head(Mibs_String_Builder *html, const char *file_path)
{
    mibs_sb_append_cstr(&alloc, html, "<head>");
    {
        generate_html_page_title(html, file_path);

        generate_html_links_and_scripts(html);
        generate_html_css(html);
    }
    mibs_sb_append_cstr(&alloc, html, "</head>");
}

// +mdoc #function          generate_html_functions
// +mdoc #note              Generate div for the function index
// +mdoc #return            void
// +mdoc #param             html/Mibs_String_Builder*
// +mdoc #param             file_path/const char*
// +mdoc #param             functions/MDoc_Functions*
void generate_html_functions(Mibs_String_Builder *html, const char* file_path, MDoc_Functions *functions)
{
    if (functions->count == 0) return;
    mibs_sb_append_cstr(&alloc, html, "<div id=\"functions\" class=\"documentation\">");
    {
        mibs_sb_append_cstr(&alloc, html, "<p class=\"text-light\">");
        {
            mibs_sb_append_cstr(&alloc, html, "Function index for file ");
            mibs_sb_append_cstr(&alloc, html, file_path);
            mibs_sb_append_cstr(&alloc, html, MIBS_NL);
        }
        mibs_sb_append_cstr(&alloc, html, "</p>");
        mibs_sb_append_cstr(&alloc, html, "<ul>");
        {
            for (size_t i = 0; i < functions->count; i++) {
                generate_html_function_doc(html, &functions->items[i]);
            }
        }
        mibs_sb_append_cstr(&alloc, html, "</ul>");
    }
    mibs_sb_append_cstr(&alloc, html, "</div>");
}

// +mdoc #function          generate_html_vars
// +mdoc #note              Generate div for the variable index
// +mdoc #return            void
// +mdoc #param             html/Mibs_String_Builder*
// +mdoc #param             file_path/const char*
// +mdoc #param             functions/MDoc_Vars*
void generate_html_vars(Mibs_String_Builder *html, const char* file_path, MDoc_Vars *vars)
{
    if (vars->count == 0) return;
    mibs_sb_append_cstr(&alloc, html, "<div id=\"functions\" class=\"documentation\">");
    {
        mibs_sb_append_cstr(&alloc, html, "<p class=\"text-light\">");
        {
            mibs_sb_append_cstr(&alloc, html, "Variable index for file ");
            mibs_sb_append_cstr(&alloc, html, file_path);
            mibs_sb_append_cstr(&alloc, html, MIBS_NL);
        }
        mibs_sb_append_cstr(&alloc, html, "</p>");
        mibs_sb_append_cstr(&alloc, html, "<ul>");
        {
            for (size_t i = 0; i < vars->count; i++) {
                generate_html_var_doc(html, &vars->items[i]);
            }
        }
        mibs_sb_append_cstr(&alloc, html, "</ul>");
    }
    mibs_sb_append_cstr(&alloc, html, "</div>");
}

// +mdoc #function          generate_html_macros
// +mdoc #note              Generate div for the macro index
// +mdoc #return            void
// +mdoc #param             html/Mibs_String_Builder*
// +mdoc #param             file_path/const char*
// +mdoc #param             macros/MDoc_Macros*
void generate_html_macros(Mibs_String_Builder *html, const char* file_path, MDoc_Macros *macros)
{
    if (macros->count == 0) return;
    mibs_sb_append_cstr(&alloc, html, "<div id=\"macros\" class=\"documentation\">");
    {
        mibs_sb_append_cstr(&alloc, html, "<p class=\"text-light\">");
        {
            mibs_sb_append_cstr(&alloc, html, "Macro index for file ");
            mibs_sb_append_cstr(&alloc, html, file_path);
            mibs_sb_append_cstr(&alloc, html, MIBS_NL);
        }
        mibs_sb_append_cstr(&alloc, html, "</p>");
        mibs_sb_append_cstr(&alloc, html, "<ul>");
        {
            for (size_t i = 0; i < macros->count; i++) {
                generate_html_macro_doc(html, &macros->items[i]);
            }
        }
        mibs_sb_append_cstr(&alloc, html, "</ul>");
    }
    mibs_sb_append_cstr(&alloc, html, "</div>");
}

// +mdoc #function          generate_html_enums
// +mdoc #note              Generate div for the enum index
// +mdoc #return            void
// +mdoc #param             html/Mibs_String_Builder*
// +mdoc #param             file_path/const char*
// +mdoc #param             enums/MDoc_Enums*
void generate_html_enums(Mibs_String_Builder *html, const char* file_path, MDoc_Enums *enums)
{
    if (enums->count == 0) return;
    mibs_sb_append_cstr(&alloc, html, "<div id=\"enums\" class=\"documentation\">");
    {
        mibs_sb_append_cstr(&alloc, html, "<p class=\"text-light\">");
        {
            mibs_sb_append_cstr(&alloc, html, "Enumeration index for file ");
            mibs_sb_append_cstr(&alloc, html, file_path);
            mibs_sb_append_cstr(&alloc, html, MIBS_NL);
        }
        mibs_sb_append_cstr(&alloc, html, "</p>");
        mibs_sb_append_cstr(&alloc, html, "<ul>");
        {
            for (size_t i = 0; i < enums->count; i++) {
                generate_html_enum_doc(html, &enums->items[i]);
            }
        }
        mibs_sb_append_cstr(&alloc, html, "</ul>");
    }
    mibs_sb_append_cstr(&alloc, html, "</div>");
}

// +mdoc #function          generate_html_structs
// +mdoc #note              Generate div for the struct index
// +mdoc #return            void
// +mdoc #param             html/Mibs_String_Builder*
// +mdoc #param             file_path/const char*
// +mdoc #param             structs/MDoc_Structs*
void generate_html_structs(Mibs_String_Builder *html, const char* file_path, MDoc_Structs *structs)
{
    if (structs->count == 0) return;
    mibs_sb_append_cstr(&alloc, html, "<div id=\"structs\" class=\"documentation\">");
    {
        mibs_sb_append_cstr(&alloc, html, "<p class=\"text-light\">");
        {
            mibs_sb_append_cstr(&alloc, html, "Structure index for file ");
            mibs_sb_append_cstr(&alloc, html, file_path);
            mibs_sb_append_cstr(&alloc, html, MIBS_NL);
        }
        mibs_sb_append_cstr(&alloc, html, "</p>");
        mibs_sb_append_cstr(&alloc, html, "<ul>");
        {
            for (size_t i = 0; i < structs->count; i++) {
                generate_html_struct_doc(html, &structs->items[i]);
            }
        }
        mibs_sb_append_cstr(&alloc, html, "</ul>");
    }
    mibs_sb_append_cstr(&alloc, html, "</div>");
}

// +mdoc #function          generate_html_searchbar
// +mdoc #note              Generate searchbar form
// +mdoc #return            void
// +mdoc #param             html/Mibs_String_Builder*
void generate_html_searchbar(Mibs_String_Builder *html)
{
    mibs_sb_append_cstr(&alloc, html, "<form id=\"searchbar_form\" class=\"input-group\">");
    {
        mibs_sb_append_cstr(&alloc, html, "<input type=\"search\" id=\"searchbar\" class=\"form-control\" />");
        mibs_sb_append_cstr(&alloc, html, "<button class=\"btn btn-secondary\" type=\"submit\">Search</button>");

    }
    mibs_sb_append_cstr(&alloc, html, "</form>");
}

// +mdoc #function          generate_html_footer
// +mdoc #note              Generate footer div
// +mdoc #return            void
// +mdoc #param             html/Mibs_String_Builder*
void generate_html_footer(Mibs_String_Builder *html)
{
    mibs_sb_append_cstr(&alloc, html, "<p id=\"footer\">");
    {
        mibs_sb_append_cstr(&alloc, html, "<hr />");
        {
            time_t now;
            struct tm ts;
            char buf[80];
            time(&now);
            ts = *localtime(&now);
            strftime(buf, sizeof(buf), "%a %Y-%m-%d %H:%M:%S %Z", &ts);
            
            mibs_sb_append_cstr(&alloc, html, "<span class=\"text-light\">Website generated by MDoc [ ");
            mibs_sb_append_cstr(&alloc, html, buf);
            mibs_sb_append_cstr(&alloc, html, " ]</span><br />");
            mibs_sb_append_cstr(&alloc, html, "<pre class=\"no-copy\"><code class=\"bash\">");
            {
                for (size_t i = 0; i < g_argc; i++) {
                    mibs_sb_append_cstr(&alloc, html, g_argv[i]);
                    if (i != g_argc-1) { mibs_sb_append_cstr(&alloc, html, " "); }
                }
            }
            mibs_sb_append_cstr(&alloc, html, "</pre></code>");
            mibs_sb_append_cstr(&alloc, html, "<div class=\"text-light\">");
            {
                mibs_sb_append_cstr(&alloc, html, "MDoc is a part of MIBS toolset. Get mibs.h <a class=\"link-info\" href=\"https://gitlab.com/kamkow1/mibs/-/raw/master/mibs.h?ref_type=heads\">here</a><br />");
                mibs_sb_append_cstr(&alloc, html, "get MDoc source code <a class=\"link-info\" href=\"https://gitlab.com/kamkow1/mibs/-/raw/master/mdoc.c?ref_type=heads\">here</a><br />");
                mibs_sb_append_cstr(&alloc, html, "check out other MIBS-related tools <a class=\"link-info\" href=\"https://gitlab.com/kamkow1/mibs/-/tree/master?ref_type=heads\">here</a><br /><br />");
                mibs_sb_append_cstr(&alloc, html, "MDoc is written and maintained by Kamil Kowalczyk (kamkow1); kamkow256@gmail.com");
            }
            mibs_sb_append_cstr(&alloc, html, "</div>");
        }
        mibs_sb_append_cstr(&alloc, html, "<hr />");
    }
    mibs_sb_append_cstr(&alloc, html, "</p>");
}

// +mdoc #function          generate_html_intro
// +mdoc #note              Generate introductory article
// +mdoc #return            void
// +mdoc #param             html/Mibs_String_Builder*
// +mdoc #param             intro/MDoc_Intro*
void generate_html_intro(Mibs_String_Builder *html, MDoc_Intro *intro)
{
    if (intro->count == 0) return;
    mibs_sb_append_null(&alloc, intro);
    mibs_sb_append_cstr(&alloc, html, "<div class=\"text-light\">");
    {
        mibs_sb_append_cstr(&alloc, html, "<h1>Introduction</h1>");
        mibs_sb_append_cstr(&alloc, html, "<p>");
        {
            mibs_sb_append_cstr(&alloc, html, intro->items);
        }
        mibs_sb_append_cstr(&alloc, html, "</p>");
    }
    mibs_sb_append_cstr(&alloc, html, "</div>");
}

// +mdoc #function          generate_html_repo
// +mdoc #note              Generate git repository link with an icon
// +mdoc #return            void
// +mdoc #param             html/Mibs_String_Builder*
// +mdoc #param             repo/MDoc_Repo*
void generate_html_repo(Mibs_String_Builder *html, MDoc_Repo *repo)
{
    if (repo->count == 0) return;

    mibs_sb_append_null(&alloc, repo);
    char *base_url = mibs_alloc(&alloc, repo->count);
    strcpy(base_url, repo->items);

    if (string_starts_with(repo->items, "https://")) {
        base_url += strlen("https://");
    } else if (string_starts_with(repo->items, "http://")) {
        base_url += strlen("https://");
    }
    char *first_slash = strchr(base_url, '/');
    int index = first_slash-base_url;
    base_url[index] = 0;

    Mibs_String_Builder favicon = {0};
    mibs_sb_append_cstr(&alloc, &favicon, "http://www.google.com/s2/favicons?domain=");
    mibs_sb_append_cstr(&alloc, &favicon, base_url);
    mibs_sb_append_null(&alloc, &favicon);

    mibs_sb_append_cstr(&alloc, html, "<a class=\"link-info\" href=");
    mibs_sb_append_cstr(&alloc, html, "\"");
    mibs_sb_append_cstr(&alloc, html, repo->items);
    mibs_sb_append_cstr(&alloc, html, "\">");
    {
        mibs_sb_append_cstr(&alloc, html, "<img src=\"");
        mibs_sb_append_cstr(&alloc, html, favicon.items);
        mibs_sb_append_cstr(&alloc, html, "\" />");

        mibs_sb_append_cstr(&alloc, html, "Project repository");
    }
    mibs_sb_append_cstr(&alloc, html, "</a>");
}

// +mdoc #function          generate_html_body
// +mdoc #note              Generate &lt;body&gt; tag and it's content
// +mdoc #return            void
// +mdoc #param             html/Mibs_String_Builder*
// +mdoc #param             file_path/const char*
// +mdoc #param             repo/MDoc_Repo*
// +mdoc #param             intro/MDoc_Intro*
// +mdoc #param             functions/MDoc_Functions*
// +mdoc #param             macros/MDoc_Macros*
// +mdoc #param             vars/MDoc_Vars*
// +mdoc #param             enums/MDoc_Enums*
// +mdoc #param             structs/MDoc_Structs*
void generate_html_body(Mibs_String_Builder *html, const char* file_path,
                        MDoc_Repo *repo, MDoc_Intro *intro,
                        MDoc_Functions *functions, MDoc_Macros *macros,
                        MDoc_Vars *vars, MDoc_Enums *enums,
                        MDoc_Structs *structs)
{
    mibs_sb_append_cstr(&alloc, html, "<body>");
    {
        generate_html_searchbar(html);
        mibs_sb_append_cstr(&alloc, html, "<div class=\"doc\">");
        {
            generate_html_repo(html, repo);
            if (repo->count > 0) mibs_sb_append_cstr(&alloc, html, "<hr />");

            generate_html_intro(html, intro);
            if (intro->count > 0) mibs_sb_append_cstr(&alloc, html, "<hr />");

            generate_html_functions(html, file_path, functions);
            if (functions->count > 0) mibs_sb_append_cstr(&alloc, html, "<hr />");

            generate_html_vars(html, file_path, vars);
            if (vars->count > 0) mibs_sb_append_cstr(&alloc, html, "<hr />");

            generate_html_enums(html, file_path, enums);
            if (enums->count > 0) mibs_sb_append_cstr(&alloc, html, "<hr />");

            generate_html_structs(html, file_path, structs);
            if (structs->count > 0) mibs_sb_append_cstr(&alloc, html, "<hr />");

            generate_html_macros(html, file_path, macros);

            generate_html_footer(html);
        }
        mibs_sb_append_cstr(&alloc, html, "</div>");

        generate_html_js_script(html);
    }
    mibs_sb_append_cstr(&alloc, html, "</body>");
}

// +mdoc #function          prepare_js_script
// +mdoc #note              Prepare JS script
// +mdoc #note              Generate an array of terms for the searchbox; Use JQueryUI for search autocompletion
// +mdoc #note              Append generated JavaScript to MDOC_JS_SCRIPT
// +mdoc #return            void
// +mdoc #param             functions/MDoc_Functions*
// +mdoc #param             macros/MDoc_Macros*
// +mdoc #param             vars/MDoc_Vars*
// +mdoc #param             enums/MDoc_Enums*
// +mdoc #param             structs/MDoc_Structs*
void prepare_js_script(MDoc_Functions *functions, MDoc_Macros *macros,
                        MDoc_Vars *vars, MDoc_Enums *enums, MDoc_Structs *structs)
{
    mibs_sb_append_cstr(&alloc, &MDOC_JS_SCRIPT, MDOC_JS_SCRIPT_BASE);
    mibs_sb_append_cstr(&alloc, &MDOC_JS_SCRIPT, "$(function() {");
    mibs_sb_append_cstr(&alloc, &MDOC_JS_SCRIPT, "var search_items = [");
    for (size_t i = 0; i < functions->count; i++) {
        mibs_sb_append_cstr(&alloc, &MDOC_JS_SCRIPT, "\"");
        mibs_sb_append_cstr(&alloc, &MDOC_JS_SCRIPT, functions->items[i].name);
        mibs_sb_append_cstr(&alloc, &MDOC_JS_SCRIPT, "\",");
    }
    for (size_t i = 0; i < macros->count; i++) {
        mibs_sb_append_cstr(&alloc, &MDOC_JS_SCRIPT, "\"");
        mibs_sb_append_cstr(&alloc, &MDOC_JS_SCRIPT, macros->items[i].name);
        mibs_sb_append_cstr(&alloc, &MDOC_JS_SCRIPT, "\",");
    }
    for (size_t i = 0; i < enums->count; i++) {
        mibs_sb_append_cstr(&alloc, &MDOC_JS_SCRIPT, "\"");
        mibs_sb_append_cstr(&alloc, &MDOC_JS_SCRIPT, enums->items[i].name);
        mibs_sb_append_cstr(&alloc, &MDOC_JS_SCRIPT, "\",");
    }
    for (size_t i = 0; i < structs->count; i++) {
        mibs_sb_append_cstr(&alloc, &MDOC_JS_SCRIPT, "\"");
        mibs_sb_append_cstr(&alloc, &MDOC_JS_SCRIPT, structs->items[i].name);
        mibs_sb_append_cstr(&alloc, &MDOC_JS_SCRIPT, "\",");
    }
    mibs_sb_append_cstr(&alloc, &MDOC_JS_SCRIPT, "];");
    mibs_sb_append_cstr(&alloc, &MDOC_JS_SCRIPT, "$('#searchbar').autocomplete({");
    mibs_sb_append_cstr(&alloc, &MDOC_JS_SCRIPT, "source: search_items");
    mibs_sb_append_cstr(&alloc, &MDOC_JS_SCRIPT, "});");
    mibs_sb_append_cstr(&alloc, &MDOC_JS_SCRIPT, "});");

    mibs_sb_append_null(&alloc, &MDOC_JS_SCRIPT);
}

// +mdoc #function          generate_html
// +mdoc #note              Generate the HTML document
// +mdoc #return            void
// +mdoc #param             html/Mibs_String_Builder*
// +mdoc #param             file_path/const char*
// +mdoc #param             repo/MDoc_Repo*
// +mdoc #param             intro/MDoc_Intro*
// +mdoc #param             functions/MDoc_Functions*
// +mdoc #param             macros/MDoc_Macros*
// +mdoc #param             vars/MDoc_Vars*
// +mdoc #param             enums/MDoc_Enums*
// +mdoc #param             structs/MDoc_Structs*
void generate_html(Mibs_String_Builder *html, const char* file_path,
                    MDoc_Repo *repo, MDoc_Intro *intro,
                    MDoc_Functions *functions, MDoc_Macros *macros,
                    MDoc_Vars *vars, MDoc_Enums *enums,
                    MDoc_Structs *structs)
{
    prepare_js_script(functions, macros, vars, enums, structs);

    mibs_sb_append_cstr(&alloc, html, "<!DOCTYPE html>");
    mibs_sb_append_cstr(&alloc, html, "<html>");
    {
        generate_html_head(html, file_path);
        generate_html_body(html, file_path, repo, intro, functions, macros, vars, enums, structs);
    }
    mibs_sb_append_cstr(&alloc, html, "</html>");

    mibs_sb_append_null(&alloc, html);
}

// +mdoc #function          process_comments
// +mdoc #note              Parse MDoc comments and generate metadata into containers:
// +mdoc #note              MDoc_Functions, MDoc_Macros, MDoc_Vars, MDoc_Intro, MDoc_Repo
// +mdoc #return            void
// +mdoc #param             html/Mibs_String_Builder*
// +mdoc #param             file_path/const char*
// +mdoc #param             input_file/Mibs_String_Builder*
void process_comments(Mibs_String_Builder *html, const char* input_file_path, Mibs_String_Builder *input_file)
{
    Current_Item_Type current_item_type;
    MDoc_Functions  functions = {0};
    MDoc_Macros     macros    = {0};
    MDoc_Vars       vars      = {0};
    MDoc_Enums      enums     = {0};
    MDoc_Structs    structs   = {0};
    MDoc_Intro      intro     = {0};
    MDoc_Repo       repo      = {0};

    Mibs_String_Array lines = mibs_split_cstr(&alloc, input_file->items, MIBS_NL);

    for (size_t i = 0; i < lines.count; i++) {
        const char* line = lines.items[i];
        if (strlen(line) > 2) {
            if (line[0] == '/' && line[1] == '/') {
                char* line_copy = mibs_alloc(&alloc, strlen(line)+1);
                strcpy(line_copy, line);
                Mibs_String_Array line_comps = mibs_split_cstr(&alloc, line_copy, " ");
                if (line_comps.count > 1) {
                    if (strcmp(line_comps.items[1], MDOC_COMMENT) == 0) {
                        if (line_comps.count < 3) {
                            mibs_log(MIBS_LL_ERROR, "%d: +mdoc comment requires a proper pragma"MIBS_NL, i+1);
                            continue;
                        }
                        const char* pragma = line_comps.items[2];
                        if (strcmp(pragma, MDOC_PRAGMA_FUNCTION) == 0) {
                            current_item_type = CITFUNCTION;
                            if (line_comps.count < 4) {
                                mibs_log(MIBS_LL_ERROR, "%d: "MDOC_PRAGMA_FUNCTION" requires a function name"MIBS_NL, i+1);
                                continue;
                            }
                            MDoc_Function function = {
                                .name = line_comps.items[3],
                                .rt_type = 0,
                                .rt_type_note = 0,
                                .note = {0},
                                .nparams = 0,
                                .params = {0},
                                .usage = {0},
                            };
                            mibs_da_append(&alloc, &functions, function);
                        } else if (strcmp(pragma, MDOC_PRAGMA_MACRO) == 0) {
                            current_item_type = CITMACRO;
                            if (line_comps.count < 4) {
                                mibs_log(MIBS_LL_ERROR, "%d: "MDOC_PRAGMA_MACRO" requires a macro name"MIBS_NL, i+1);
                                continue;
                            }
                            MDoc_Macro macro = {
                                .name = line_comps.items[3],
                                .rt_type = 0,
                                .rt_type_note = 0,
                                .note = {0},
                                .nparams = 0,
                                .params = {0},
                                .usage = {0},
                            };
                            mibs_da_append(&alloc, &macros, macro);
                        } else if (strcmp(pragma, MDOC_PRAGMA_PARAM) == 0) {
                            char* begin = (char*)line + strlen("//") + 1 + strlen("+mdoc") +1 + strlen(MDOC_PRAGMA_PARAM) + 1;
                            while(isspace(*begin++));
                            begin--;
                            Mibs_String_Array param_type_pair = mibs_split_cstr(&alloc, begin, "/");

                            MDoc_Function_Param param = {
                                .name = param_type_pair.items[0],
                                .type = param_type_pair.items[1],
                            };
                            if (current_item_type == CITFUNCTION) {
                                MDoc_Function* function = &functions.items[functions.count-1];
                                function->params[function->nparams++] = param;
                            } else if (current_item_type == CITMACRO) {
                                MDoc_Macro* macro = &macros.items[macros.count-1];
                                macro->params[macro->nparams++] = param;
                            }
                        } else if (strcmp(pragma, MDOC_PRAGMA_RETURN) == 0) {
                            char* begin = (char*)line + strlen("//") + 1 + strlen("+mdoc") +1 + strlen(MDOC_PRAGMA_RETURN) + 1;
                            while(isspace(*begin++));
                            begin--;
                            if (current_item_type == CITFUNCTION) {
                                MDoc_Function* function = &functions.items[functions.count-1];
                                function->rt_type = begin;
                            } else if (current_item_type == CITMACRO) {
                                MDoc_Macro* macro = &macros.items[macros.count-1];
                                macro->rt_type = begin;
                            }
                        } else if (strcmp(pragma, MDOC_PRAGMA_RETURN_NOTE) == 0) {
                            char* begin = (char*)line + strlen("//") + 1 + strlen("+mdoc") +1 + strlen(MDOC_PRAGMA_RETURN_NOTE) + 1;
                            while(isspace(*begin++));
                            begin--;
                            if (current_item_type == CITFUNCTION) {
                                MDoc_Function *function = &functions.items[functions.count-1];
                                function->has_notes = true;
                                function->rt_type_note = begin;
                            } else if (current_item_type == CITMACRO) {
                                MDoc_Macro *macro = &macros.items[macros.count-1];
                                macro->has_notes = true;
                                macro->rt_type_note = begin;
                            }
                        } else if (strcmp(pragma, MDOC_PRAGMA_PARAM_NOTE) == 0) {
                            char* begin = (char*)line + strlen("//") + 1 + strlen("+mdoc") +1 + strlen(MDOC_PRAGMA_PARAM_NOTE) + 1;
                            while(isspace(*begin++));
                            begin--;
                            if (current_item_type == CITFUNCTION) {
                                MDoc_Function *function = &functions.items[functions.count-1];
                                function->has_notes = true;
                                function->params[function->nparams-1].param_note = begin;
                            } else if (current_item_type == CITMACRO) {
                                MDoc_Macro *macro = &macros.items[macros.count-1];
                                macro->has_notes = true;
                                macro->params[macro->nparams-1].param_note = begin;
                            }
                        } else if (strcmp(pragma, MDOC_PRAGMA_NOTE) == 0) {
                            char* begin = (char*)line + strlen("//") + 1 + strlen("+mdoc") +1 + strlen(MDOC_PRAGMA_NOTE) + 1;
                            if (current_item_type == CITFUNCTION) {
                                MDoc_Function *function = &functions.items[functions.count-1];
                                mibs_sb_append_cstr(&alloc, &function->note, begin);
                                mibs_sb_append_cstr(&alloc, &function->note, "<br />");
                                function->has_notes = true;
                            } else if (current_item_type == CITMACRO) {
                                MDoc_Macro *macro = &macros.items[macros.count-1];
                                mibs_sb_append_cstr(&alloc, &macro->note, begin);
                                mibs_sb_append_cstr(&alloc, &macro->note, "<br />");
                                macro->has_notes = true;
                            } else if (current_item_type == CITVAR) {
                                MDoc_Var *var = &vars.items[vars.count-1];
                                mibs_sb_append_cstr(&alloc, &var->note, begin);
                                mibs_sb_append_cstr(&alloc, &var->note, "<br />");
                            } else if (current_item_type == CITENUM) {
                                MDoc_Enum *_enum = &enums.items[enums.count-1];
                                mibs_sb_append_cstr(&alloc, &_enum->note, begin);
                                mibs_sb_append_cstr(&alloc, &_enum->note, "<br />");
                            } else if (current_item_type == CITSTRUCT) {
                                MDoc_Struct *_struct = &structs.items[structs.count-1];
                                mibs_sb_append_cstr(&alloc, &_struct->note, begin);
                                mibs_sb_append_cstr(&alloc, &_struct->note, "<br />");
                            } else {
                                mibs_log(MIBS_LL_ERROR, MDOC_PRAGMA_NOTE": Unhandled item type %d"MIBS_NL, current_item_type);
                            }
                        } else if (strcmp(pragma, MDOC_PRAGMA_USAGE) == 0) {
                            char* begin = (char*)line + strlen("//") + 1 + strlen("+mdoc") +1 + strlen(MDOC_PRAGMA_USAGE) + 1;
                            if (current_item_type == CITFUNCTION) {
                                MDoc_Function *function = &functions.items[functions.count-1];
                                mibs_sb_append_cstr(&alloc, &function->usage, begin);
                                mibs_sb_append_cstr(&alloc, &function->usage, MIBS_NL);
                            } else if (current_item_type == CITMACRO) {
                                MDoc_Macro *macro = &macros.items[macros.count-1];
                                mibs_sb_append_cstr(&alloc, &macro->usage, begin);
                                mibs_sb_append_cstr(&alloc, &macro->usage, MIBS_NL);
                            } else if (current_item_type == CITVAR) {
                                MDoc_Var *var = &vars.items[vars.count-1];
                                mibs_sb_append_cstr(&alloc, &var->usage, begin);
                                mibs_sb_append_cstr(&alloc, &var->usage, MIBS_NL);
                            } else if (current_item_type == CITENUM) {
                                MDoc_Enum *_enum = &enums.items[enums.count-1];
                                mibs_sb_append_cstr(&alloc, &_enum->usage, begin);
                                mibs_sb_append_cstr(&alloc, &_enum->usage, MIBS_NL);
                            } else if (current_item_type == CITSTRUCT) {
                                MDoc_Struct *_struct = &structs.items[structs.count-1];
                                mibs_sb_append_cstr(&alloc, &_struct->usage, begin);
                                mibs_sb_append_cstr(&alloc, &_struct->usage, MIBS_NL);
                            } else {
                                mibs_log(MIBS_LL_ERROR, MDOC_PRAGMA_USAGE": Unhandled item type %d"MIBS_NL, current_item_type);
                            }
                        } else if (strcmp(pragma, MDOC_PRAGMA_VARIADIC) == 0) {
                            if (current_item_type == CITFUNCTION) {
                                MDoc_Function *function = &functions.items[functions.count-1];
                                function->is_variadic = true;
                            } else if (current_item_type == CITMACRO) {
                                MDoc_Macro *macro = &macros.items[macros.count-1];
                                macro->is_variadic = true;
                            }
                        } else if (strcmp(pragma, MDOC_PRAGMA_FUNCLIKE) == 0) {
                                MDoc_Macro *macro = &macros.items[macros.count-1];
                                macro->is_funclike = true;
                        } else if (strcmp(pragma, MDOC_PRAGMA_INTRO) == 0) {
                            char* begin = (char*)line + strlen("//") + 1 + strlen("+mdoc") +1 + strlen(MDOC_PRAGMA_INTRO) + 1;
                            mibs_sb_append_cstr(&alloc, &intro, begin);
                            mibs_sb_append_cstr(&alloc, &intro, MIBS_NL);
                        } else if (strcmp(pragma, MDOC_PRAGMA_REPO) == 0) {
                            char* begin = (char*)line + strlen("//") + 1 + strlen("+mdoc") +1 + strlen(MDOC_PRAGMA_REPO) + 1;
                            mibs_sb_append_cstr(&alloc, &repo, begin);
                        } else if (strcmp(pragma, MDOC_PRAGMA_VAR) == 0) {
                            current_item_type = CITVAR;
                            char* begin = (char*)line + strlen("//") + 1 + strlen("+mdoc") +1 + strlen(MDOC_PRAGMA_PARAM) + 1;
                            while(isspace(*begin++));
                            begin--;
                            Mibs_String_Array name_type_pair = mibs_split_cstr(&alloc, begin, "/");
                            
                            MDoc_Var var = {0};
                            var.name = name_type_pair.items[0];
                            var.type = name_type_pair.items[1];

                            mibs_da_append(&alloc, &vars, var);
                        } else if (strcmp(pragma, MDOC_PRAGMA_ENUM) == 0) {
                            current_item_type = CITENUM;
                            char* begin = (char*)line + strlen("//") + 1 + strlen("+mdoc") +1 + strlen(MDOC_PRAGMA_ENUM) + 1;
                            while(isspace(*begin++));
                            begin--;

                            MDoc_Enum _enum = {0};
                            _enum.name = begin;

                            mibs_da_append(&alloc, &enums, _enum);
                        } else if (strcmp(pragma, MDOC_PRAGMA_ENUM_ITEM) == 0) {
                            char* begin = (char*)line + strlen("//") + 1 + strlen("+mdoc") +1 + strlen(MDOC_PRAGMA_ENUM_ITEM) + 1;
                            while(isspace(*begin++));
                            begin--;

                            MDoc_Enum_Item item = {0};
                            item.name = begin;

                            MDoc_Enum *_enum = &enums.items[enums.count-1];
                            _enum->items[_enum->nitems++] = item;
                        } else if (strcmp(pragma, MDOC_PRAGMA_ENUM_ITEM_NOTE) == 0) {
                            char* begin = (char*)line + strlen("//") + 1 + strlen("+mdoc") +1 + strlen(MDOC_PRAGMA_ENUM_ITEM_NOTE) + 1;
                            while(isspace(*begin++));
                            begin--;

                            MDoc_Enum *_enum = &enums.items[enums.count-1];
                            MDoc_Enum_Item *item = &_enum->items[_enum->nitems-1];
                            mibs_sb_append_cstr(&alloc, &item->note, begin);
                            mibs_sb_append_cstr(&alloc, &item->note, "<br />");
                        } else if (strcmp(pragma, MDOC_PRAGMA_STRUCT) == 0) {
                            current_item_type = CITSTRUCT;
                            char* begin = (char*)line + strlen("//") + 1 + strlen("+mdoc") +1 + strlen(MDOC_PRAGMA_STRUCT) + 1;
                            while(isspace(*begin++));
                            begin--;
                            
                            MDoc_Struct _struct = {0};
                            _struct.name = begin;

                            mibs_da_append(&alloc, &structs, _struct);
                        } else if (strcmp(pragma, MDOC_PRAGMA_STRUCT_FIELD) == 0) {
                            char* begin = (char*)line + strlen("//") + 1 + strlen("+mdoc") +1 + strlen(MDOC_PRAGMA_STRUCT_FIELD) + 1;
                            while(isspace(*begin++));
                            begin--;
                            Mibs_String_Array name_type_pair = mibs_split_cstr(&alloc, begin, "/");

                            MDoc_Struct_Field field = {
                                .name = name_type_pair.items[0],
                                .type = name_type_pair.items[1],
                            };

                            MDoc_Struct *_struct = &structs.items[structs.count-1];
                            _struct->fields[_struct->nfields++] = field;
                        } else if (strcmp(pragma, MDOC_PRAGMA_STRUCT_FIELD_NOTE) == 0) {
                            char* begin = (char*)line + strlen("//") + 1 + strlen("+mdoc") +1 + strlen(MDOC_PRAGMA_STRUCT_FIELD_NOTE) + 1;
                            while(isspace(*begin++));
                            begin--;

                            MDoc_Struct *_struct = &structs.items[structs.count-1];
                            MDoc_Struct_Field *field = &_struct->fields[_struct->nfields-1];
                            mibs_sb_append_cstr(&alloc, &field->note, begin);
                            mibs_sb_append_cstr(&alloc, &field->note, "<br />");
                        } else {
                            mibs_log(MIBS_LL_ERROR, "Unknown pragma %s"MIBS_NL, pragma);
                        }
                    }
                }
            }
        }
    }
    generate_html(html, input_file_path, &repo, &intro, &functions, &macros, &vars, &enums, &structs);
}

static Mibs_Arg args[] = {
    { .id = 'i', .expeced_type = MIBS_AT_STRING, .description = "path to input file" },
    { .id = 'o', .expeced_type = MIBS_AT_STRING, .description = "path to output file" }
};

// +mdoc #function          main
// +mdoc #note              Program options:
// +mdoc #note              &lt;input file path&gt; [ required ]
// +mdoc #note              &lt;output file path&gt; [ required ]
// +mdoc #return            int
// +mdoc #param             argc/int
// +mdoc #param             argv/char**
int main(int argc, char ** argv)
{
    g_argc = argc;
    g_argv = argv;
    mibs_rebuild(&alloc, argc, argv);

    mibs_parse_args(argc, argv, args);
    Mibs_Arg *arg_i = mibs_get_arg(args, 'i');
    Mibs_Arg *arg_o = mibs_get_arg(args, 'o');
    const char* input_path = arg_i->value.s;
    const char* output_path = arg_o->value.s;

    Mibs_Read_File_Result rfr = mibs_read_file(&alloc, input_path);
    if (!rfr.ok) { return 1; }
    Mibs_String_Builder input_file_content = rfr.value;
   
    Mibs_String_Builder html = {0};
    process_comments(&html, input_path, &input_file_content);
    FILE *o = fopen(output_path, "w");
    if (!o) { return 1; }
    mdefer { fclose(o); }
    fwrite(html.items, html.count-1, 1, o);

    mibs_arena_cleanup(&alloc);

    return 0;
}

