# mibs.h

Mibs - Mini build system

mibs.h is a simple, header-only C library/framework for creating user-space applications. Simple and type-safe by design.

## The build system

A minimal example of the mibs build system looks like this:

```c
// Compile with cc -o build_system_example build_system_example.c
#define MIBS_IMPL
#include "mibs.h"

int main(int argc, char** argv)
{
    Mibs_Default_Allocator allocator = mibs_make_default_allocator();
    mibs_rebuild(&allocator, argc, argv);

    Mibs_Cmd build1_cmd = {0};
    Mibs_Cmd build2_cmd = {0};

    // allocator_example.c
    mibs_cmd_append(&allocator, &build1_cmd, MIBS_CC);
    mibs_cmd_append(&allocator, &build1_cmd, "-o", "allocator_example");
    mibs_cmd_append(&allocator, &build1_cmd, "allocator_example.c");
    mibs_run_cmd(&allocator, &build1_cmd, MIBS_CMD_SYNC, NULL);

    // dir_example.c
    mibs_cmd_append(&allocator, &build2_cmd, MIBS_CC);
    mibs_cmd_append(&allocator, &build2_cmd, "-o", "dir_example");
    mibs_cmd_append(&allocator, &build2_cmd, "dir_example.c");
    mibs_run_cmd(&allocator, &build2_cmd, MIBS_CMD_SYNC, NULL);

    mibs_da_deinit(&allocator, &build1_cmd);
    mibs_da_deinit(&allocator, &build2_cmd);

    return 0;
}
```

The build system can rebuild itself. Just save the file and re-run the executable.

## Type safety

Mibs focuses on type safety using C preprocessor macros. A simple example is mibs' result type:

```c
#define Mibs_Result(TValue) struct { bool ok; TValue value; }
```

which can be later used like this:

```
Mibs_Result(int) result = get_int();
if (!result.ok) handle_error();
do_something_with_the_value(result.value);
```

another example are dynamic arrays:

```c
#define Mibs_Da(T) struct { T* items; size_t count; size_t cap; }
```

```c

typedef Mibs_Da(char) Dynamic_String;

Dynamic_String ds = {0};

mibs_da_append(&allocator, &ds, 'a');
mibs_da_append(&allocator, &ds, 'b');
mibs_da_append(&allocator, &ds, 'c');
mibs_da_append(&allocator, &ds, '\0');

printf("ds.items = %s\n", ds.items);

mibs_da_deinit(&allocator, &ds);
```

## Allocators

Mibs supports different memory allocation strategies:
- Fixed Buffer Allocator
- Linear Allocator

It's very easy to extend mibs and add your own allocator. As an example, let's take a look
at Mibs\_Fixed\_Buffer\_Allocator:

```c
typedef struct {
    size_t cursor;
    size_t max;
    size_t allocation_count;
    void* buffer;
} Mibs_Fixed_Buffer_Underlying;

typedef Mibs_Allocator(Mibs_Fixed_Buffer_Underlying) Mibs_Fixed_Buffer_Allocator;

#define mibs_make_fixed_buffer_allocator(_buffer, _max) \
    ({ \
        Mibs_Fixed_Buffer_Allocator fb = {0}; \
        Mibs_Fixed_Buffer_Underlying fb_underlying = (Mibs_Fixed_Buffer_Underlying){ \
            .max = _max, \
            .allocation_count = _buffer != NULL ? 1 : 0, \
            .buffer = _buffer \
        }; \
        fb.underlying = fb_underlying; \
        fb.base.alloc_func = mibs_fixed_buffer_alloc; \
        fb.base.free_func = mibs_fixed_buffer_free; \
        fb.base.realloc_func = mibs_fixed_buffer_realloc; \
        fb; \
    })

#define mibs_make_fixed_buffer_allocator_owned(_buffer, _max) \
    ({ \
        Mibs_Fixed_Buffer_Allocator fb = {0}; \
        Mibs_Fixed_Buffer_Underlying fb_underlying = (Mibs_Fixed_Buffer_Underlying){ \
            .max = _max, \
            .allocation_count = 0, \
            .buffer = _buffer \
        }; \
        fb.underlying = fb_underlying; \
        fb.base.alloc_func = mibs_fixed_buffer_alloc; \
        fb.base.free_func = mibs_fixed_buffer_free; \
        fb.base.realloc_func = mibs_fixed_buffer_realloc; \
        fb; \
    })
// See mibs.h for full implementation
void* mibs_fixed_buffer_alloc(Mibs_Fixed_Buffer_Allocator* fb, size_t size);
void mibs_fixed_buffer_free(Mibs_Fixed_Buffer_Allocator* fb, void* mem);
void* mibs_fixed_buffer_realloc(Mibs_Fixed_Buffer_Allocator* fb, void* mem, size_t prev_size, size_t new_size);
```

## Cross-platform

Mibs is cross-platform and works with all major compilers (GCC, MSVC, Clang).

