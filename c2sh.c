#define MIBS_IMPL
#include "mibs.h"

// Config
// -------------------------------------------------------
const char *C2SH_CONFIG_H[] = {
    "./kdb/kdb.h",
    "./kdb/db.h",
};
const char *C2SH_CONFIG_C[] = {
    "./kdb/kdb.c",
    "./kdb/db.c",
};
#define C2SH_CONFIG_NAME "kdb"
// -------------------------------------------------------

Mibs_Default_Allocator alloc = mibs_make_default_allocator();

void string_upper(const char *s, char *out)
{
    const char *s2 = s;
    size_t i = 0;
    while(*s2) {
        out[i++] = toupper(*s2++);
    }
}

int main(int argc, char ** argv)
{
    mibs_rebuild(&alloc, argc, argv);

    Mibs_String_Builder sb = {0};
    mdefer { mibs_da_deinit(&alloc, &sb); }
    
    char c2sh_config_name_upper[sizeof(C2SH_CONFIG_NAME)+1];
    memset(c2sh_config_name_upper, 0, sizeof(c2sh_config_name_upper));
    string_upper(C2SH_CONFIG_NAME, c2sh_config_name_upper);
    Mibs_String_Builder inc_guard = {0};
    mdefer { mibs_da_deinit(&alloc, &inc_guard); }
    mibs_sb_append_cstr(&alloc, &inc_guard, c2sh_config_name_upper);
    mibs_sb_append_cstr(&alloc, &inc_guard, "_H_");
    mibs_sb_append_null(&alloc, &inc_guard);
   
    mibs_sb_append_cstr(&alloc, &sb, "#ifndef ");
    mibs_sb_append_cstr(&alloc, &sb, inc_guard.items);
    mibs_sb_append_cstr(&alloc, &sb, "\n");
    mibs_sb_append_cstr(&alloc, &sb, "#define ");
    mibs_sb_append_cstr(&alloc, &sb, inc_guard.items);
    mibs_sb_append_cstr(&alloc, &sb, "\n");
    
    for (size_t i = 0; i < mibs_array_len(C2SH_CONFIG_H); i++) {
        const char* h = C2SH_CONFIG_H[i];
        Mibs_Read_File_Result rfr = mibs_read_file(&alloc, h);
        if (!rfr.ok) { continue; }
        Mibs_String_Builder content = rfr.value;
        mdefer { mibs_da_deinit(&alloc, &content); }
        mibs_sb_append_cstr(&alloc, &sb, "// ");
        mibs_sb_append_cstr(&alloc, &sb, h);
        mibs_sb_append_cstr(&alloc, &sb, "\n");
        mibs_sb_append_cstr(&alloc, &sb, "// ====================================================================\n");
        mibs_sb_append_cstr(&alloc, &sb, content.items);
        mibs_sb_append_cstr(&alloc, &sb, "// ====================================================================\n");
    }

    mibs_sb_append_cstr(&alloc, &sb, "#ifdef ");
    mibs_sb_append_cstr(&alloc, &sb, c2sh_config_name_upper);
    mibs_sb_append_cstr(&alloc, &sb, "_IMPL");
    mibs_sb_append_cstr(&alloc, &sb, "\n");
   
    for (size_t i = 0; i < mibs_array_len(C2SH_CONFIG_C); i++) {
        const char* c = C2SH_CONFIG_C[i];
        Mibs_Read_File_Result rfr = mibs_read_file(&alloc, c);
        if (!rfr.ok) { continue; }
        Mibs_String_Builder content = rfr.value;
        mdefer { mibs_da_deinit(&alloc, &content); }
        mibs_sb_append_cstr(&alloc, &sb, "// ====================================================================\n");
        mibs_sb_append_cstr(&alloc, &sb, content.items);
        mibs_sb_append_cstr(&alloc, &sb, "// ====================================================================\n");
    }

    mibs_sb_append_cstr(&alloc, &sb, "#endif // ");
    mibs_sb_append_cstr(&alloc, &sb, c2sh_config_name_upper);
    mibs_sb_append_cstr(&alloc, &sb, "_IMPL");
    mibs_sb_append_cstr(&alloc, &sb, "\n");
    
    mibs_sb_append_cstr(&alloc, &sb, "#endif // ");
    mibs_sb_append_cstr(&alloc, &sb, inc_guard.items);
    mibs_sb_append_cstr(&alloc, &sb, "\n");

    Mibs_String_Builder path = {0};
    mdefer { mibs_da_deinit(&alloc, &path); }
    mibs_sb_append_cstr(&alloc, &path, C2SH_CONFIG_NAME);
    mibs_sb_append_cstr(&alloc, &path, ".h");
    mibs_sb_append_null(&alloc, &path);

    FILE *f = fopen(path.items, "w");
    if (!f) return 1;
    fwrite(sb.items, sb.count, 1, f);
    fclose(f);

    return 0;
}
