//
// This is a simple file watcher that will re-run your mibs script
// whenever a file changes in your project!!
//
// Usage: file_watcher <your binary> <files...>
//

#define MIBS_IMPL
#include "mibs.h"
#ifdef MIBS_PLATFORM_WINDOWS
#   undef MIBS_CC
#   define MIBS_CC "C:\\MinGW\\bin\\gcc.exe"
#endif

typedef Mibs_Da(const char*) File_List;

#if defined(MIBS_PLATFORM_UNIX)
#define build_mibs() \
({ \
    Mibs_Cmd _cmd = {0}; \
    mdefer { mibs_da_deinit(&alloc, &_cmd); } \
    mibs_cmd_append(&alloc, &_cmd, MIBS_CC, "-o", "mibs", "./mibs.c"); \
    mibs_run_cmd(&alloc, &_cmd, MIBS_CMD_SYNC, 0).ok; \
})
#elif defined(MIBS_PLATFORM_WINDOWS)
#define build_mibs() \
({ \
    Mibs_Cmd _cmd = {0}; \
    mdefer { mibs_da_deinit(&alloc, &_cmd); } \
    mibs_cmd_append(&alloc, &_cmd, MIBS_CC, "-o", "mibs.exe", ".\\mibs.c"); \
    mibs_run_cmd(&alloc, &_cmd, MIBS_CMD_SYNC, 0).ok; \
})
#else
#   error "Unknown platform"
#endif

int main(int argc, char ** argv)
{
    if (argc < 2) {
        mibs_log(MIBS_LL_ERROR, "Too few arguments\n");
        return 1;
    }
    const char* program = argv[1];

    Mibs_Default_Allocator alloc = mibs_make_default_allocator();
    File_List file_list = {0};
    mdefer{ mibs_da_deinit(&alloc, &file_list); }

    for (size_t i = 2; i < argc; i++) {
        mibs_da_append(&alloc, &file_list, argv[i]);
    }

    if (mibs_path_exists("files.fw")) {
        Mibs_Read_File_Result fr = mibs_read_file(&alloc, "files.fw");
        Mibs_String_Array lines = mibs_split_cstr(&alloc, fr.value.items, "\n");
        for (size_t i = 0; i < lines.count; i++) {
            if (lines.items[i][0] != '#') {
                mibs_da_append(&alloc, &file_list, lines.items[i]);
            }
        }
    }

#if defined(MIBS_PLATFORM_UNIX)
    if (!mibs_path_exists("mibs")) {
#elif defined(MIBS_PLATFORM_WINDOWS)
    if (!mibs_path_exists("mibs.exe")) {
#else
#   error "Unknown platform"
#endif
        if (!build_mibs()) return 1;
    }

    if (!mibs_path_exists(program)) {
        Mibs_Cmd cmd = {0};
        mdefer { mibs_da_deinit(&alloc, &cmd); }
#if defined(MIBS_PLATFORM_UNIX)
        mibs_cmd_append(&alloc, &cmd, "./mibs");
#elif defined(MIBS_PLATFORM_WINDOWS)
        mibs_cmd_append(&alloc, &cmd, ".\\mibs.exe");
#else
#   error "Unknown platform"
#endif
        mibs_run_cmd(&alloc, &cmd, MIBS_CMD_SYNC, 0);
    }
    while(1) {
        mibs_rebuild(&alloc, argc, argv);
        for (size_t i = 0; i < file_list.count; i++) {
            if(mibs_needs_rebuild(program, file_list.items[i])) {
                mibs_log(MIBS_LL_INFO, "RECOMPILING (%s)\n", file_list.items[i]);
                Mibs_Cmd cmd = {0};
                mdefer { mibs_da_deinit(&alloc, &cmd); }
#if defined(MIBS_PLATFORM_UNIX)
                mibs_cmd_append(&alloc, &cmd, "./mibs");
#elif defined(MIBS_PLATFORM_WINDOWS)
                build_mibs();
                mibs_cmd_append(&alloc, &cmd, ".\\mibs.exe");
#else
#   error "Unknown platform"
#endif
                mibs_run_cmd(&alloc, &cmd, MIBS_CMD_SYNC, 0);
                break;
            }
        }
    }
    return 0;
}

