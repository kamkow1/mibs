#ifndef MDG_H_
#define MDG_H_

// Definitions for generated source code by MDG (Metadata generator)

typedef struct {
   const char *name;
   const char *type;
} Field_MD;

typedef Field_MD Field_MDs[0xff];

typedef struct {
   const char *name;
   int nfields;
   Field_MDs fields;
} Struct_MD;

typedef struct {
    const char *name;
    const char *type;
} Param_MD;

typedef Param_MD Param_MDs[0xff];

typedef struct {
    const char *name;
    const char *rt_type;
    int nparams;
    Param_MDs params;
    int is_variadic;
} Func_MD;

// Uitility macros

// get generated name
#define MD(X) X##_MD

//
// Usage: mdg_struct_to_string(&alloc, MyAwesomeStruct)
//
#define mdg_struct_to_string(alloc, strct) \
({ \
    Mibs_String_Builder _s = {0}; \
    mibs_sb_append_cstr((alloc), &_s, #strct); \
    mibs_sb_append_cstr((alloc), &_s, " { "); \
    for (size_t _i = 0; _i < MD(strct).nfields; _i++) { \
        mibs_sb_append_cstr((alloc), &_s, MD(strct).fields[_i].type); \
        mibs_sb_append_cstr((alloc), &_s, " "); \
        mibs_sb_append_cstr((alloc), &_s, MD(strct).fields[_i].name); \
        if (_i != MD(strct).nfields-1) { \
            mibs_sb_append_cstr((alloc), &_s, ", "); \
        } \
    } \
    mibs_sb_append_cstr((alloc), &_s, " }"); \
    mibs_sb_append_null((alloc), &_s); \
    _s; \
})

//
// Usage: mdg_structv_to_string(&alloc, "MyAwesomeStruct")
//
#define mdg_structv_to_string(alloc, strctv) \
({ \
    Mibs_String_Builder _s = {0}; \
    mibs_sb_append_cstr((alloc), &_s, (strctv)->name); \
    mibs_sb_append_cstr((alloc), &_s, " { "); \
    for (size_t _i = 0; _i < (strctv)->nfields; _i++) { \
        mibs_sb_append_cstr((alloc), &_s, (strctv)->fields[_i].type); \
        mibs_sb_append_cstr((alloc), &_s, " "); \
        mibs_sb_append_cstr((alloc), &_s, (strctv)->fields[_i].name); \
        if (_i != (strctv)->nfields-1) { \
            mibs_sb_append_cstr((alloc), &_s, ", "); \
        } \
    } \
    mibs_sb_append_cstr((alloc), &_s, " }"); \
    mibs_sb_append_null((alloc), &_s); \
    _s; \
})

// Compare struct types only by field types
#define mdg_cmp_struct_types(s1, s2) \
({ \
    bool _ok = false; \
    for (size_t _i = 0; _i < MD(s1).nfields; _i++) { \
        for (size_t _j = 0; _j < MD(s2).nfields; _j++) { \
            _ok = strcmp(MD(s1).fields[_i].type, MD(s2).fields[_j].type) == 0; \
        } \
    } \
    _ok; \
})

// Compare struct types semantically, taking field names into account
#define mdg_cmp_struct_sem(s1, s2) \
({ \
    bool _ok = false; \
    for (size_t _i = 0; _i < MD(s1).nfields; _i++) { \
        for (size_t _j = 0; _j < MD(s2).nfields; _j++) { \
            _ok = strcmp(MD(s1).fields[_i].type, MD(s2).fields[_j].type) == 0; \
            _ok = strcmp(MD(s1).fields[_i].name, MD(s2).fields[_j].name) == 0; \
        } \
    } \
    _ok; \
})

#define mdg_get_func_md(file, _name) \
({ \
    Func_MD *_md = 0; \
    for (size_t _i = 0; _i < mibs_array_len(file##_md_h_func_mds); _i++) { \
        if (strcmp(file##_md_h_func_mds[_i]->name, _name) == 0) _md = file##_md_h_func_mds[_i]; \
    } \
    _md; \
})

#define mdg_get_struct_md(file, _name) \
({ \
    Struct_MD *_md = 0; \
    for (size_t _i = 0; _i < mibs_array_len(file##_md_h_struct_mds); _i++) { \
        if (strcmp(file##_md_h_struct_mds[_i]->name, _name) == 0) _md = file##_md_h_struct_mds[_i]; \
    } \
    _md; \
})

#define mdg_func_names(file) file##_md_h_func_names
#define mdg_struct_names(file) file##_md_h_struct_names

#endif // MDG_H_
