#!/bin/sh
. ./ftp_credentials.sh

set -xe

MIBS=./mibs.html
MDOC=./mdoc.html

ftp -n <<EOF
open $FTP_HOST
user $FTP_USER $FTP_PASSWORD
put $MIBS
put $MDOC
EOF

